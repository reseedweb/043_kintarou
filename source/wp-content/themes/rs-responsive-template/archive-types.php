<?php get_header(); ?>
<div class="primary-row clearfix">	
	<div class="row clearfix">
        <div class="col-lg-18 col-md-18 col-sm-18 col-xs-18">
			<h2 class="h2-sp-title">
			<img src="<?php bloginfo('template_url'); ?>/img/content/type_top_img.jpg" alt="卓上カレンダー" />
			<span class="h2-sp-text">卓上カレンダー</span>
		</h2>
		</div>
	</div>	
</div><!-- end primary-row -->

<div class="primary-row clearfix">	
	<div class="row clearfix">
        <div class="col-lg-18 col-md-18 col-sm-18 col-xs-18">			
			<ul class="type-tabs-detail"><!-- begin type-tabs-detail-->
				<li><a href="<?php bloginfo('url'); ?>/types">ALL</a></li>					
				<li><a href="<?php bloginfo('url'); ?>/cat-types/ケース">ケース</a></li>					
				<li><a href="<?php bloginfo('url'); ?>/cat-types/三角スタンド">三角スタンド</a></li>
				<li><a href="<?php bloginfo('url'); ?>/cat-types/テレフォン">テレフォン</a></li>									
			</ul><!-- end type-tabs-detail -->
		</div>
	</div>

	<?php	
		$type_posts = get_posts( array(
			'post_type'=> 'types',				
			$paged = get_query_var( 'paged' ) ? get_query_var( 'paged' ) : 1,
			'posts_per_page' => 6,
			'paged' => $paged,				  
		));
	?>	 	
	<?php $i = 0;?>
	<?php foreach($type_posts as $type_post):?>
	<?php $i++; ?>
	<?php if($i%3 == 1) : ?>			
	<div class="row type-content clearfix">        		
	<?php endif; ?>	
		<div class="col-lg-6 col-md-6 col-sm-6 col-xs-18 xs-arrange">				
			<div class="type-info clearfix">					
				<p class="type-info-img"><?php echo get_the_post_thumbnail($type_post->ID,'medium'); ?></p>
				<h4 class="type-info-title"><?php @the_terms($type_post->ID, 'cat-types'); ?></h4>
				<p class="type-info-text1"><a href="<?php echo get_the_permalink($type_post->ID); ?>"><?php echo $type_post->post_title; ?></a></p>
					<div class="type-info-text2 clearfix">
						<div class="type-info-size1"><span class="type-info-size">size</span><?php echo get_field('text2', $type_post->ID); ?></div>
						<div class="type-info-size2"><span class="type-info-size">price</span><span class="type-info-symbol1">@</span><?php echo get_field('text3', $type_post->ID); ?><span class="type-info-symbol2">円</span></div>
					</div>                    								
			</div>
		</div><!-- end col -->		
	<?php if($i%3 == 0 || $i == count($type_posts) ) : ?>				
	</div><!-- end row -->
	<?php endif; ?>
	<?php endforeach; ?> 			
	
	<div class="primary-row text-center">
	    <?php if(function_exists('wp_pagenavi')) {  wp_pagenavi();} ?>
	</div>	
	<?php wp_reset_query(); ?>
</div><!-- end primary-row -->	

<?php get_template_part('part','flow'); ?>    
<?php get_footer(); ?>