	<div class="primary-row clearfix"><!-- begin primary-row -->                                                          		                                                          	                                                                                                                                         	                          
		<div class="row clearfix">
            <div class="col-lg-18 col-md-18 col-sm-18 col-xs-18">
            	<h2 class="h2-title">
					<img src="<?php bloginfo('template_url'); ?>/img/common/h2_title.png" alt="<?php bloginfo('name'); ?>" />
					<div class="h2-title-text">お問合せ、お見積りのご依頼はコチラから</div>
				</h2>
			</div>		
		</div><!-- end row -->
		
		<div class="row clearfix">
            <div class="col-lg-18 col-md-18 col-sm-18 col-xs-18">
            	<div class="top-flow clearfix">
					<a href="<?php bloginfo('url'); ?>/flow">
						<img src="<?php bloginfo('template_url'); ?>/img/top/top_flow_img.png" alt="<?php bloginfo('name'); ?>" />
					</a>
            		<div class="top-flow-text">お問合せから納品までの流れ</div>
            	</div>
            	
            	<div class="top-contact  xs-arrange clearfix">
	            	<img src="<?php bloginfo('template_url'); ?>/img/top/top_contact_img.png" alt="<?php bloginfo('name'); ?>" />
	            	<div class="top-contact-text">お気軽にお問い合わせください</div>
	            	<div class="top-contact-btn">
	            		<a href="<?php bloginfo('url'); ?>/contact">
	            			<img src="<?php bloginfo('template_url'); ?>/img/top/top_contact_btn.jpg" alt="<?php bloginfo('name'); ?>" />
	            		</a>
	            	</div>
	            </div>            	
            </div>
        </div><!-- end row -->		
	</div><!-- end primary-row -->