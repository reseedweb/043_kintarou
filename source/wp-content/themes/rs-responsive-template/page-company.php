<?php get_header(); ?>
<div class="primary-row clearfix">	
	<div class="row clearfix">
        <div class="col-lg-18 col-md-18 col-sm-18 col-xs-18">
			<h2 class="h2-sp-title">
			<img src="<?php bloginfo('template_url'); ?>/img/content/company_top_img.jpg" alt="会社概要" />
			<span class="h2-sp-text">会社概要</span>
		</h2>
		</div>		
	</div>	
</div><!-- end primary-row -->

<div class="primary-row clearfix"><!-- begin primary-row -->
	<div class="row clearfix">
        <div class="col-lg-18 col-md-18 col-sm-18 col-xs-18">
			<h3 class="title-sub">
				<img src="<?php bloginfo('template_url'); ?>/img/content/title_sub.png" alt="会社概要" />
				<div class="title-sub-text">会社概要</div>
			</h3>
		</div>		
	</div>	

	<div class="row clearfix">
        <div class="col-lg-18 col-md-18 col-sm-18 col-xs-18">
        	<table class="company-table-content">
				<tbody>
					<tr>
						<th>会社名</th>
						<td>卓上名入れカレンダー.com</td>
					</tr>
					<tr>
						<th>所在地</th>
						<td>〒583-0037 大阪府藤井寺市津堂2-13-25</td>
					</tr>
					<tr>
						<th>TEL</th>
						<td>072-959-3311</td>
					</tr>
					<tr>
						<th>FAX</th>
						<td>072-959-3321</td>
					</tr>
					<tr>
						<th>代表</th>
						<td>谷口 圭代</td>
					</tr>
					<tr>
						<th>アクセス</th>
						<td>
							<p>【電車の場合】<br />
							近鉄南大阪線、藤井寺駅北口より近鉄バスで大正橋南詰めで下車徒歩5分です。<br />
							藤井寺駅よりお電話いただければ、駅まで車で迎えに行きます。<br />
							大阪市営地下鉄、谷町線八尾南南口より近鉄バスで大正橋南詰めで下車徒歩5分です。<br />
							八尾南駅よりお電話いただければ、駅まで車で迎えに行きます。<br />
							【自動車の場合】<br />
							西名阪藤井寺インターチェンジから5分<br />
							※駐車場あります
							</p>
						</td>
					</tr>
				</tbody>
			</table>
        </div>
   	</div>								
</div><!-- end primary-row -->

<div class="primary-row clearfix"><!-- begin primary-row -->
	<div class="row clearfix">
        <div class="col-lg-18 col-md-18 col-sm-18 col-xs-18">
			<h3 class="title-sub">
				<img src="<?php bloginfo('template_url'); ?>/img/content/title_sub.png" alt="アクセス" />
				<div class="title-sub-text">アクセス</div>
			</h3>
		</div>		
	</div>	

	<div class="row clearfix">
        <div class="col-lg-18 col-md-18 col-sm-18 col-xs-18">
        	<iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3284.7846614767595!2d135.5878245!3d34.5843149!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x6000d88912953797%3A0xb45ea1b67c77c6!2s2+Chome-13-25+Tsud%C5%8D%2C+Fujiidera-shi%2C+%C5%8Csaka-fu+583-0037%2C+Japan!5e0!3m2!1sen!2s!4v1439976443108" width="750" height="250" frameborder="0" style="border:0" allowfullscreen></iframe>
        </div>
    </div>
</div><!-- end primary-row -->


<?php get_template_part('part','flow'); ?>  

<?php get_footer(); ?>