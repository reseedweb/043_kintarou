<!DOCTYPE html>
<html>
    <head>
        <!-- meta -->        
        <!--<meta http-equiv="X-UA-Compatible" content="IE=Edge,chrome=1" />-->
		<meta http-equiv="X-UA-Compatible" content="IE=Edge" />
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />         
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <!-- title -->
		<title><?php if(is_home()){ echo bloginfo("name"); echo " | "; echo bloginfo("description"); } else { echo wp_title(" | ", false, right); echo bloginfo("name"); } ?> </title>
		<!-- <title>?php wp_title(''); ?></title>	-->
        <meta name="robots" content="noindex,follow,noodp" />
        
        <link rel="profile" href="http://gmpg.org/xfn/11" />        
        <!--<link rel="shortcut icon" href="" />-->
        
        <!-- global javascript variable -->
        <script type="text/javascript">
            var CONTAINER_WIDTH = '1080px';
            var CONTENT_WIDTH = '1060px';
            var BASE_URL = '<?php bloginfo('url'); ?>';
            var TEMPLATE_URI = '<?php bloginfo('template_url') ?>';
            var CURRENT_MODULE_URI = '';
            Date.now = Date.now || function() { return +new Date; };            
        </script>        
        <!-- Bootstrap -->
        <link href="<?php bloginfo('template_url'); ?>/css/bootstrap.min.css" rel="stylesheet" />
        <link href="<?php bloginfo('template_url'); ?>/css/bootstrap-theme.min.css" rel="stylesheet" />   
        <!-- fontawesome -->
        <link href="<?php bloginfo('template_url'); ?>/fonts/font-awesome/css/font-awesome.min.css" rel="stylesheet" />

        <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
        <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
        <!--[if lt IE 9]>
        <script src="<?php bloginfo('template_url'); ?>/js/html5shiv.js"></script>
        <script src="<?php bloginfo('template_url'); ?>/js/respond.min.js"></script>
        <![endif]-->        
        		
		<!-- Jquery -->
		<script src="<?php bloginfo('template_url'); ?>/js/jquery.js" type="text/javascript"></script>		        	
		<script src="<?php bloginfo('template_url'); ?>/js/breakpoints_heightLine.js" type="text/javascript"></script>
		<script src="<?php bloginfo('template_url'); ?>/js/jquery.reseed.js" type="text/javascript"></script>				
		<script src="<?php bloginfo('template_url'); ?>/js/jquery.waypoints.min.js" type="text/javascript"></script>		
		<script src="<?php bloginfo('template_url'); ?>/js/jquery.sidr.min.js" type="text/javascript"></script>		
		<script src="http://ajaxzip3.googlecode.com/svn/trunk/ajaxzip3/ajaxzip3.js" charset="UTF-8"></script>			
		<script src="<?php bloginfo('template_url'); ?>/js/tgImageChangeV2.js" type="text/javascript"></script>		
		
		<!-- CSS -->				
		<link href="<?php bloginfo('template_url'); ?>/style.css?<?php echo md5(date('l jS \of F Y h:i:s A')); ?>" rel="stylesheet" />          		
		<link href="<?php bloginfo('template_url'); ?>/css/jquery.sidr.light.css" rel="stylesheet" />			
        <?php wp_head(); ?>		
    </head>
    <body>     
        <div id="screen_type"></div>
        <div id="wrapper"><!-- begin wrapper -->
			<div id="top-header">
				<div id="top-header-info">
					<section id="top">
						<div class="container"><!-- begin container -->
							<div class="row clearfix"><!-- begin row -->
								<div class="col-lg-9 col-md-9 col-sm-9 col-xs-9 pull-left">
									<h1 class="top-text">オリジナル卓上カレンダー専門店「卓上名入れカレンダー.com」＜2015年度版＞</h1>							
								</div>
								<div class="col-lg-9 col-md-9 col-sm-9 col-xs-9 pull-right">
									<ul class="topnavi">
										<li>
											<a href="<?php bloginfo('url'); ?>/about"><i class="fa fa-tags"></i>はじめての方へ</a>
										</li>
										<li>
											<a href="<?php bloginfo('url'); ?>/flow"><i class="fa fa-file"></i>ご利用の流れ</a>
										</li>
										<li>											
											<a href="<?php bloginfo('url'); ?>/faq"><i class="fa fa-comments"></i>よくあるご質問</a>
										</li>
										<li>											
											<a href="<?php bloginfo('url'); ?>/company"><i class="fa fa-building"></i>会社概要</a>
										</li>
									</ul>
								</div>
							</div><!-- end row -->
						</div><!-- end container --> 										
					</section>
							
					<header><!-- begin header -->
						<div id="header"><!-- begin header -->
							<div class="container clearfix"><!-- begin container -->
								<div id="headerContent" class="row add-sp container-head clearfix"><!-- begin row -->							
									<div class="col-lg-9 col-md-9 col-sm-9 col-xs-12 pull-left"><!-- begin col -->                        
										<?php if(is_front_page()) : ?>
											<h1 class="logo">
												<a href="<?php bloginfo('url'); ?>">
													<img src="<?php bloginfo('template_url'); ?>/img/common/logo.jpg" alt="<?php bloginfo('name'); ?>" />
												</a>
											</h1>
										<?php else : ?>
											<div class="logo"><a href="<?php bloginfo('url'); ?>"><img src="<?php bloginfo('template_url'); ?>/img/common/logo.jpg" alt="<?php bloginfo('name'); ?>" /></a></div>
										<?php endif; ?>
									</div>
									<div class="col-lg-9 col-md-9 col-sm-9 col-xs-6 pull-right">
										<div class="header-contact invisible-sp clearfix">
											<div class="header-tel"><img src="<?php bloginfo('template_url'); ?>/img/common/header_tel.jpg" alt="<?php bloginfo('name'); ?>" /></div>
											<div class="header-con"><a href="<?php bloginfo('url'); ?>/contact"><img src="<?php bloginfo('template_url'); ?>/img/common/header_con.jpg" alt="<?php bloginfo('name'); ?>" /></a></div>
										</div>	
												
										<div class="header-right visible-sp">
											<a id="right-menu" href="#right-menu"><i class="fa fa-align-justify"></i></a>
											<a href="tel:0753127665"><i class="fa fa-phone-square"></i></a>											
											<div id="navi-sp">
												<ul>
													<li><a href="<?php bloginfo('url'); ?>"><i class="fa fa-home"></i>トップページ</a></li>										
													<li><a href="<?php bloginfo('url'); ?>/types"><i class="fa fa-calendar-o"></i>卓上カレンダー</a></li>
													<li><a href="<?php bloginfo('url'); ?>/kabekake"><i class="fa fa-calendar"></i>壁掛けカレンダー</a></li>
													<li><a href="<?php bloginfo('url'); ?>/original"><i class="fa fa-jpy"></i>オリジナルカレンダー</a></li>
													<li><a href="<?php bloginfo('url'); ?>/draft"><i class="fa fa-folder-open"></i>データ入稿</a></li>
													<li><a href="<?php bloginfo('url'); ?>/about"><i class="fa fa-calculator"></i>初めてご利用される方へ</a></li>
													<li><a href="<?php bloginfo('url'); ?>/flow"><i class="fa fa-calculator"></i>ご注文の流れ</a></li>
													<li><a href="<?php bloginfo('url'); ?>/faq"><i class="fa fa-calculator"></i>よくあるご質問</a></li>
													<li><a href="<?php bloginfo('url'); ?>/work"><i class="fa fa-calculator"></i>製作実績</a></li>
													<li><a href="<?php bloginfo('url'); ?>/price"><i class="fa fa-calculator"></i>料金表</a></li>
													<li><a href="<?php bloginfo('url'); ?>/estimate"><i class="fa fa-calculator"></i>お見積り</a></li>
													<li><a href="<?php bloginfo('url'); ?>/factory"><i class="fa fa-calculator"></i>工場紹介</a></li>
													<li><a href="<?php bloginfo('url'); ?>/company"><i class="fa fa-calculator"></i>会社概要</a></li>
													<li><a href="<?php bloginfo('url'); ?>/contact"><i class="fa fa-calculator"></i>お問合せ</a></li>
													<li><a href="<?php bloginfo('url'); ?>/privacy"><i class="fa fa-calculator"></i>プライバシーポリシー</a></li>
												</ul>
											</div>											
										</div>												
									</div>
									<!-- end col -->  																	
								</div><!-- end row -->
							</div><!-- end container -->        
						</div><!-- end header -->
					</header><!-- end header -->
				</div>		           		
				<?php get_template_part('part','nav'); ?>						
			</div>
			
			<?php if(is_home()) : ?>
				<?php get_template_part('part','slider'); ?>
				<?php get_template_part('part','topflow'); ?>                
				<?php get_template_part('part','topmain1'); ?> 
				<?php get_template_part('part','topmain2'); ?>
			<?php else :?>
				<?php get_template_part('part','breadcrumb'); ?>
            <?php endif; ?>		
						
            <section id="content"><!-- begin content -->
				<div class="container"><!-- begin container -->
                    <div class="row clearfix"><!-- begin row -->
						<div class="col-lg-5 col-md-5 col-sm-5" id="sibar-pc"><!-- begin coll -->
                            <aside id="sidebar">
                                <?php get_template_part('sidebar'); ?> 
                            </aside><!-- end sidebar -->
                        </div><!-- end col --> 
                        <div class="col-lg-13 col-md-13 col-sm-13 col-xs-18"><!-- begin col -->
							<main id="primary">