			<div id="page-feature">						
				<div class="page-feature-content clearfix">					
					<?php if(is_page('pallet')) : ?>		
						<img src="<?php bloginfo('template_url'); ?>/img/content/pallet_top_img.jpg" alt="top" />
						<div class="container"><!-- begin container -->
							<h2 class="feature-title"><?php the_title(); ?></h2>
						</div><!-- end container -->	
						
					<?php elseif(is_page('company')) : ?>						
						<img src="<?php bloginfo('template_url'); ?>/img/content/company_top_img.jpg" alt="top" />
						<div class="container"><!-- begin container -->
							<h2 class="feature-title"><?php the_title(); ?></h2>
						</div><!-- end container -->
						
					<?php elseif(is_page('cardboard')) : ?>						
						<img src="<?php bloginfo('template_url'); ?>/img/content/pladan_top_img.jpg" alt="top" />
						<div class="container"><!-- begin container -->
							<h2 class="feature-title"><?php the_title(); ?></h2>
						</div><!-- end container -->
					
					<?php elseif(is_page('package')) : ?>						
						<img src="<?php bloginfo('template_url'); ?>/img/content/pladan_top_img.jpg" alt="top" />
						<div class="container"><!-- begin container -->
							<h2 class="feature-title"><?php the_title(); ?></h2>
						</div><!-- end container -->
						
					<?php elseif(is_page('pladan')) : ?>						
						<img src="<?php bloginfo('template_url'); ?>/img/content/pladan_top_img.jpg" alt="top" />
						<div class="container"><!-- begin container -->
							<h2 class="feature-title"><?php the_title(); ?></h2>
						</div><!-- end container -->
						
					<?php elseif(is_page('kyoto')) : ?>						
						<img src="<?php bloginfo('template_url'); ?>/img/content/workshop_top_img.jpg" alt="top" />
						<div class="container"><!-- begin container -->
							<h2 class="feature-title"><?php the_title(); ?></h2>
						</div><!-- end container -->
					
					<?php elseif(is_page('jyoyo')) : ?>						
						<img src="<?php bloginfo('template_url'); ?>/img/content/workshop_top_img.jpg" alt="top" />
						<div class="container"><!-- begin container -->
							<h2 class="feature-title"><?php the_title(); ?></h2>>
						</div><!-- end container -->
						
					<?php elseif(is_page('okayama')) : ?>						
						<img src="<?php bloginfo('template_url'); ?>/img/content/workshop_top_img.jpg" alt="top" />
						<div class="container"><!-- begin container -->
							<h2 class="feature-title"><?php the_title(); ?></h2>
						</div><!-- end container -->
					
					<?php elseif(is_page('mie')) : ?>						
						<img src="<?php bloginfo('template_url'); ?>/img/content/workshop_top_img.jpg" alt="top" />
						<div class="container"><!-- begin container -->
							<h2 class="feature-title"><?php the_title(); ?></h2>
						</div><!-- end container -->						
					 
					<?php elseif(is_page('hukui')) : ?>						
						<img src="<?php bloginfo('template_url'); ?>/img/content/workshop_top_img.jpg" alt="top" />
						<div class="container"><!-- begin container -->
							<h2 class="feature-title"><?php the_title(); ?></h2>
						</div><!-- end container -->
						
					<?php elseif(is_page('shiga')) : ?>						
						<img src="<?php bloginfo('template_url'); ?>/img/content/workshop_top_img.jpg" alt="top" />
						<div class="container"><!-- begin container -->
							<h2 class="feature-title"><?php the_title(); ?></h2>
						</div><!-- end container -->
						
					<?php elseif(is_page('strength')) : ?>						
						<img src="<?php bloginfo('template_url'); ?>/img/content/strength_top_img.jpg" alt="top" />
						<div class="container"><!-- begin container -->
							<h2 class="feature-title"><?php the_title(); ?></h2>
						</div><!-- end container -->
						
					<?php elseif(is_page('greeting')) : ?>						
						<img src="<?php bloginfo('template_url'); ?>/img/content/greeting_top_img.jpg" alt="top" />
						<div class="container"><!-- begin container -->
							<h2 class="feature-title"><?php the_title(); ?></h2>
						</div><!-- end container -->
					
					<?php elseif(is_page('history')) : ?>						
						<img src="<?php bloginfo('template_url'); ?>/img/content/history_top_img.jpg" alt="top" />
						<div class="container"><!-- begin container -->
							<h2 class="feature-title"><?php the_title(); ?></h2>
						</div><!-- end container -->
						
					<?php elseif(is_page('quality')) : ?>						
						<img src="<?php bloginfo('template_url'); ?>/img/content/quality_top_img.jpg" alt="top" />
						<div class="container"><!-- begin container -->
							<h2 class="feature-title"><?php the_title(); ?></h2>
						</div><!-- end container -->
						
					<?php elseif(is_page('contact')) : ?>						
						<img src="<?php bloginfo('template_url'); ?>/img/content/contact_top_img.jpg" alt="top" />
						<div class="container"><!-- begin container -->
							<h2 class="feature-title"><?php the_title(); ?></h2>
						</div><!-- end container -->					
					<?php endif; ?>  
				</div><!-- end page-feature-content -->
			</div><!-- end page-feature -->   

			<div id="sub-navi-content">
				<?php if(is_page('greeting') || is_page('company') || is_page('history') || is_page('quality')):?>		
					<?php get_template_part('part','subnavi');?>					
				<?php endif; ?> 
			</div>			
			<?php get_template_part('part','breadcrumb');?>