<?php get_header();?>
<div class="primary-row clearfix">	
	<div class="row clearfix">
        <div class="col-lg-18 col-md-18 col-sm-18 col-xs-18">
			<h2 class="title-sub">
				<img src="<?php bloginfo('template_url'); ?>/img/content/title_sub.png" alt="タイプを見る" />
				<div class="title-sub-text">壁掛けカレンダー</div>
			</h2>
		</div>
	</div>	
	
	<div class="row mt20 clearfix">
        <div class="col-lg-18 col-md-18 col-sm-18 col-xs-18">			
			<ul class="kabekake-tabs-detail"><!-- begin kabekake-tabs-detail --->
				<li><a href="<?php bloginfo('url'); ?>/kabekake">ALL</a></li>					
				<li><a href="<?php bloginfo('url'); ?>/cat-kabekake/文字月表">文字月表</a></li>					
				<li><a href="<?php bloginfo('url'); ?>/cat-kabekake/写真月表">写真月表</a></li>
				<li><a href="<?php bloginfo('url'); ?>/cat-kabekake/絵柄月表">絵柄月表</a></li>									
			</ul><!-- end kabekake-tabs-detail -->
		</div>
	</div>
		
	<?php
		$queried_object = get_queried_object();
   	 	$term_id = $queried_object->term_id;	
		$kabekake_posts = get_posts( array(
	    'post_type'=> 'kabekake',
        'posts_per_page' => get_query_var('posts_per_page'),
        'paged' => get_query_var('paged'),		                
        'tax_query' => array(
            array(
            'taxonomy' => 'cat-kabekake', 
            'field' => 'term_id', 
            'terms' => $term_id))
			
	));
	?>
	
	<div class="row clearfix">
					<div class="col-lg-18 col-md-18 col-sm-18 col-xs-18">
						<h3 class="type-dtitle-h31">
							<img src="<?php bloginfo('template_url'); ?>/img/content/type_dtitle_h31.jpg" alt="タイプを見る" />
							<div class="type-h31-text"><?php @the_terms($kabekake_post->ID, 'cat-kabekake'); ?></div>
						</h3>
					</div>
				</div>
				<?php $i = 0;?>
				<?php foreach($kabekake_posts as $kabekake_post):?>
				<?php $i++; ?>
				<?php if($i%3 == 1) : ?>			
				<div class="row kabekake-content clearfix">        		
				<?php endif; ?>	
					<div class="col-lg-6 col-md-6 col-sm-6 col-xs-18 xs-arrange">				
						<div class="kabekake-info clearfix">					
							<p class="kabekake-info-img"><?php echo get_the_post_thumbnail($kabekake_post->ID,'medium'); ?></p>
							<h4 class="kabekake-info-title"><?php @the_terms($kabekake_post->ID, 'cat-kabekake'); ?></h4>
							<p class="kabekake-info-text1"><a href="<?php echo get_the_permalink($kabekake_post->ID); ?>"><?php echo $kabekake_post->post_title; ?></a></p>
								<div class="kabekake-info-text2 clearfix">
									<div class="kabekake-info-size1"><span class="kabekake-info-size">size</span><?php echo get_field('text2', $kabekake_post->ID); ?></div>
									<div class="kabekake-info-size2"><span class="kabekake-info-size">price</span><span class="kabekake-info-symbol1">@</span><?php echo get_field('text3', $kabekake_post->ID); ?><span class="kabekake-info-symbol2">円</span></div>
								</div>                    								
						</div>
					</div><!-- end col -->		
				<?php if($i%3 == 0 || $i == count($kabekake_posts) ) : ?>				
				</div><!-- end row -->
				<?php endif; ?>
				<?php endforeach; ?> 
	  	
	  	
	<div class="primary-row text-center">
		<?php if(function_exists('wp_pagenavi')) { wp_pagenavi(); } ?>
	</div>
	<?php wp_reset_query(); ?>		
		
</div><!-- end primary-row -->

<?php get_template_part('part','flow'); ?>    
<?php get_footer();?>