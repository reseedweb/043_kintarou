<?php get_header(); ?>                   
    <?php if (have_posts()) : ?>
    <?php while (have_posts()) : the_post(); ?>    
        <!-- do stuff ... -->
		<div class="primary-row clearfix">
			<div class="row clearfix">
				<div class="col-lg-18 col-md-18 col-sm-18 col-xs-18">
					<h2 class="title-sub">
						<img src="<?php bloginfo('template_url'); ?>/img/content/title_sub.png" alt="お問い合わせ" />
						<div class="title-sub-text"><?php the_title(); ?></div>
					</h2>
					<div class="post-row-content">                
						<div class="post-row-meta">
							<i class="fa fa-clock-o"></i><?php the_time('l, F jS, Y'); ?>
							<i class="fa fa-tags"></i><?php the_category(' , ', get_the_id()); ?>
							<i class="fa fa-user"></i><span style="color:red;"><?php the_author_link(); ?></span>
						</div>
						<div class="post-row-description"><?php the_content(); ?></div>                    
					</div><!-- end post-row-content -->
				</div>		
			</div>        
		</div>
        <?php endwhile; ?>    
        <div class="primary-row clearfix">
            <?php if(function_exists('wp_pagenavi')) wp_pagenavi(); ?>
        </div>
    <?php endif; ?>
    <?php wp_reset_query(); ?>    
<?php get_template_part('part','flow');?>
<?php get_footer(); ?>