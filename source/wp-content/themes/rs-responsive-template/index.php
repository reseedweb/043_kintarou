<?php get_header(); ?>
    <div class="primary-row clearfix"><!-- begin primary-row -->                                                                                       
        <div class="row clearfix">
            <div class="col-lg-18 col-md-18 col-sm-18 col-xs-18">
            	<div class="top-content1 clearfix">
            		<img src="<?php bloginfo('template_url'); ?>/img/top/top_content1_img.jpg" alt="<?php bloginfo('name'); ?>" />
            		<div class="top-content1-text1"><span class="top-font">お申込日</span>4/1～5/31</div>
            		<div class="top-content1-text2"><span class="top-font">お申込日</span>6/1～7/31</div>
            	</div>               	
			</div>		
		</div><!-- end row -->			
	</div><!-- end primary-row -->
	
	<div class="primary-row clearfix"><!-- begin primary-row -->                                                                                       
		<div class="row clearfix">
            <div class="col-lg-18 col-md-18 col-sm-18 col-xs-18">
            	<h2 class="h2-title">
					<img src="<?php bloginfo('template_url'); ?>/img/common/h2_title.png" alt="<?php bloginfo('name'); ?>" />
					<div class="h2-title-text">卓上名入れカレンダー.comが選ばれる<span class="h2-title-clr">3つの理由</span></div>
				</h2>
			</div>		
		</div><!-- end row -->

		<div class="row clearfix">
			<div class="col-lg-6 col-md-6 col-sm-6 col-xs-18">
				<article class="top-content2 clearfix">
					<p class="top-content2-img"><img src="<?php bloginfo('template_url'); ?>/img/top/top_content2_img1.jpg" alt="<?php bloginfo('name'); ?>" /></p>					
					<h3 class="top-content2-title">確かな技術力でどんなものにも印刷します</h3 >
					<p class="top-content2-text xs-arrange">卓上名入れカレンダー.comでは、特殊印刷の技術で曲面や凹凸面、布地やプラスチック、ガラスなど、どこにでも印刷することもでき、小さな文字もきれいに印刷できます。</p>					
				</article>
			</div>
			
			<div class="col-lg-6 col-md-6 col-sm-6 col-xs-18">
				<article class="top-content2 clearfix">
					<p class="top-content2-img"><img src="<?php bloginfo('template_url'); ?>/img/top/top_content2_img2.jpg" alt="<?php bloginfo('name'); ?>" /></p>					
					<h3 class="top-content2-title">小ロット・短納期で柔軟柔軟に対応しております</h3 >
					<p class="top-content2-text xs-arrange">卓上名入れカレンダー.comでは、安定した品質の製品を1冊000円～という安価、最短○週間～という短納期で製作することができます！サンプル製作も承っております。 </p>					
				</article>
			</div>
			
			<div class="col-lg-6 col-md-6 col-sm-6 col-xs-18">
				<article class="top-content2 clearfix">
					<p class="top-content2-img"><img src="<?php bloginfo('template_url'); ?>/img/top/top_content2_img3.jpg" alt="<?php bloginfo('name'); ?>" /></p>					
					<h3 class="top-content2-title">経験豊富なオーナーが直接ご提案いたします！</h3 >
					<p class="top-content2-text">実現できるか分からないことや、こんな商品に印刷できるだろうか？と思ったら、まずご相談ください。サンプル印刷からお引き受けいたします。</p>					
				</article>
			</div>
		</div><!-- end row -->
	</div><!-- end primary-row -->

	<div class="primary-row clearfix"><!-- begin primary-row -->                                                          		                                                          	                                                                                                                                         	                          
		<div class="row clearfix">
            <div class="col-lg-18 col-md-18 col-sm-18 col-xs-18">
            	<h2 class="h2-title">
					<img src="<?php bloginfo('template_url'); ?>/img/common/h2_title.png" alt="<?php bloginfo('name'); ?>" />
					<div class="h2-title-text">初めてご利用される方へ</div>
				</h2>
			</div>		
		</div><!-- end row -->

		<div class="row clearfix">
            <div class="col-lg-18 col-md-18 col-sm-18 col-xs-18">            	
            	<a href="<?php bloginfo('url'); ?>/about">
            		<div class="top-content3 clearfix">
	            		<img src="<?php bloginfo('template_url'); ?>/img/top/top_content3_img.png" alt="<?php bloginfo('name'); ?>" />
	            		<div class="top-content3-text1">カレンダーの製作・名入れは<br />お任せください！</div>
	            		<div class="top-content3-text2">卓上名入れカレンダー.comはカレンダー製作の専門会社です。<br />初めてご利用されるお客様をご案内いたします！</div>
	            		<div class="top-content3-text3">低価格・高品質ってホント？</div>
	            		<div class="top-content3-text4">急ぎの案件だけど間に合う？</div>
	            		<div class="top-content3-text5">繊細な色味にしたいんだけど？</div>
	            		<div class="top-content3-text6">どうやって選んだらいいの？</div>
	            	</div>
            	</a>            	
            </div>
        </div><!-- end row -->
	</div><!-- end primary-row -->


	<div class="primary-row clearfix"><!-- begin primary-row -->                                                          		                                                          	                                                                                                                                         	                          
		<div class="row clearfix">
            <div class="col-lg-18 col-md-18 col-sm-18 col-xs-18">
            	<h2 class="h2-title">
					<img src="<?php bloginfo('template_url'); ?>/img/common/h2_title.png" alt="<?php bloginfo('name'); ?>" />
					<div class="h2-title-text">自社工場のご紹介</div>
				</h2>
			</div>		
		</div><!-- end row -->

		<div class="row clearfix">
            <div class="col-lg-18 col-md-18 col-sm-18 col-xs-18">            	
            	<a href="<?php bloginfo('url'); ?>/factory">
            		<div class="top-content4 clearfix">
	            		<img src="<?php bloginfo('template_url'); ?>/img/top/top_content4_img.png" alt="<?php bloginfo('name'); ?>" />
	            		<div class="top-content4-text1">卓上名入れカレンダー.comの<br />自社工場をご紹介します！</div>
	            		<div class="top-content4-text2">卓上名入れカレンダー.comが保有する<br />自社生産工場の様子と機械設備を<br />ご紹介いたします！</div>
	            	</div>
            	</a>            	
            </div>
        </div><!-- end row -->
	</div><!-- end primary-row -->

	
	<?php get_template_part('part','flow');?>

	
	<div class="primary-row clearfix"><!-- begin primary-row -->                                                          		                                                          	                                                                                                                                         	                          
		<div class="row clearfix">
            <div class="col-lg-18 col-md-18 col-sm-18 col-xs-18">
            	<h2 class="h2-title">
					<img src="<?php bloginfo('template_url'); ?>/img/common/h2_title.png" alt="<?php bloginfo('name'); ?>" />
					<div class="h2-title-text">代表者からのメッセージ</div>
				</h2>
			</div>		
		</div><!-- end row -->

		<div class="row clearfix">
            <div class="col-lg-6 col-md-6 col-sm-6 col-xs-18 xs-align">
            	<p><img src="<?php bloginfo('template_url'); ?>/img/top/top_intro_img.jpg" alt="<?php bloginfo('name'); ?>" /></p>
			</div>		
			<div class="col-lg-12 col-md-12 col-sm-12 col-xs-18 top-info-text xs-arrange">
				<p>はじめまして、本サイトをご覧いただき、ありがとうございます。弊社は創業からカレンダー製作を通して、「お客様のご要望を叶える」ことをモットーにお客様とともに歩んでまいりました。</p>
				<p>近年、IT化が進む中、販促業界の盛り上がりとともにカレンダーの需要も変わってまいりました。</p>
				<p>そのような需要の中、中国などで安価なカレンダーを製作するということが多くなってきております。しかし、残念なことに、価格が安いだけで質や対応が悪かったり、要望と違うものが完成品として、送られてきたりといった問題のある業者も少なくありません。</p>
				<p>その点、弊社では小ロット・短納期・高品質・丁寧な対応など、「年間00万冊の製作実績」と「自社工場を保有」しているからこそできる強みがあります。</p>
				<p>名入れカレンダーを製作されたいという方は、是非「卓上名入れカレンダー.com」にお気軽にご相談ください。</p>
			</div>
		</div><!-- end row -->				
	</div><!-- end primary-row -->

<?php get_footer(); ?>