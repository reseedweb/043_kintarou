<?php get_header(); ?>
<div class="primary-row clearfix"><!-- begin primary-row -->
	<div class="row clearfix">
        <div class="col-lg-18 col-md-18 col-sm-18 col-xs-18">
			<h2 class="h2-sp-title">
				<img src="<?php bloginfo('template_url'); ?>/img/content/estimate_top_img.jpg" alt="お問い合わせ" />
				<span class="h2-sp-text">お問い合わせ</span>
			</h2>			
		</div>		
	</div>
	<div class="row clearfix">
        <div class="col-lg-18 col-md-18 col-sm-18 col-xs-18">
			<?php echo do_shortcode('[contact-form-7 id="92" title="Japanese-Contact-Form"]') ?>
		</div>		
	</div>
</div><!-- end primary-row -->

<?php get_template_part('part','flow'); ?>
<?php get_footer(); ?>