<?php get_header();?>
<div class="primary-row clearfix">	
	<div class="row clearfix">
        <div class="col-lg-18 col-md-18 col-sm-18 col-xs-18">
			<h2 class="h2-sp-title">
			<img src="<?php bloginfo('template_url'); ?>/img/content/factory_top_img.jpg" alt="工場紹介" />
			<span class="h2-sp-text">工場紹介</span>
		</h2>
		</div>		
	</div>	
	<div class="row clearfix">
		<div class="col-lg-18 col-md-18 col-sm-18 col-xs-18 mt20">
			<p>こちらではカレンダー以外にも特殊印刷を扱っている弊社の工場を紹介します。<br />どんなものにも印刷しますので、お気軽にお問い合わせください</p>
		</div>
	</div>
</div><!-- end primary-row -->


<div class="primary-row clearfix">	
	<div class="row clearfix">
        <div class="col-lg-18 col-md-18 col-sm-18 col-xs-18">
			<h3 class="title-sub">
				<img src="<?php bloginfo('template_url'); ?>/img/content/title_sub.png" alt="工場紹介" />
				<div class="title-sub-text">特殊印刷の動画一例</div>
			</h3>
		</div>
	</div>
	
	<div class="row clearfix">	
		<div class="col-lg-9 col-md-9 col-sm-9 col-xs-18 xs-arrange">
			<div class="factory-content clearfix">
				<p class="factory-cont-video"><iframe width="100%" height="100%" src="https://www.youtube.com/embed/6kpMk0f40MM" frameborder="0" allowfullscreen></iframe></p>
				<h4 class="factory-cont-title">特殊印刷1</h4>
				<p class="factory-cont-text">こちらではカレンダー以外にも特殊印刷を扱っている弊社の工場を紹介します。<br />どんなものにも印刷しますので、お気軽にお問い合わせください</p>
			</div>			
		</div>
		<div class="col-lg-9 col-md-9 col-sm-9 col-xs-18">
			<div class="factory-content clearfix">
				<p class="factory-cont-video"><iframe width="100%" height="100%" src="https://www.youtube.com/embed/KGkQrZK5krs" frameborder="0" allowfullscreen></iframe></p>
				<h4 class="factory-cont-title">特殊印刷2</h4>
				<p class="factory-cont-text">こちらではカレンダー以外にも特殊印刷を扱っている弊社の工場を紹介します。<br />どんなものにも印刷しますので、お気軽にお問い合わせください</p>
			</div>			
		</div>		
	</div>
</div><!-- end primary-row -->

<?php get_template_part('part','flow'); ?>  

<?php get_footer();?>