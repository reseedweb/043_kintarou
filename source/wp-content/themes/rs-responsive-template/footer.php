							</main><!-- end primary -->
                        </div><!-- end col -->
                        <div class="col-xs-18" id="sibar-sp"><!-- begin coll -->
                            <aside id="sidebar">
                                <?php if(is_page('blog') || is_category() || is_single()) : ?>
								<?php
									$queried_object = get_queried_object();                                
									$sidebar_part = 'blog';
									if(is_tax() || is_archive()){                                    
										$sidebar_part = '';
									}                               
									if($queried_object->post_type != 'post' && $queried_object->post_type != 'page'){
										$sidebar_part = '';
									}   
									if($queried_object->taxonomy == 'category'){                                    
										$sidebar_part = 'blog';
									}                 
								?>
								<?php get_template_part('sidebar',$sidebar_part); ?>  
								<?php else: ?>
									<?php get_template_part('sidebar'); ?>  
								<?php endif; ?> 
                            </aside><!-- end sidebar -->
                        </div><!-- end col -->                   
                    </div><!-- end row -->
                </div><!-- end container -->
            </section><!-- end content -->
           
			<footer id="footer"><!-- begin footer -->
				<div id="footer-top"><!-- begin footer-top -->            
					<div class="container clearfix"><!-- begin container -->
						<div class="row footer-info1 clearfix">
							<div class="col-lg-9 col-md-9 col-sm-9 col-xs-18">							
								<a class="footer-logo" href="<?php bloginfo('url'); ?>">
									<img src="<?php bloginfo('template_url'); ?>/img/common/footer_logo.jpg" alt="<?php bloginfo('name'); ?>" />
									<span class="footer-title">オリジナル卓上カレンダー専門店「卓上名入れカレンダー.com」＜2015年度版＞</span>
								</a>
							</div>
							
							<div class="col-lg-9 col-md-9 col-sm-9 col-xs-18">
								<div class="footer-tel"><img src="<?php bloginfo('template_url'); ?>/img/common/footer_tel.jpg" alt="<?php bloginfo('name'); ?>" /></div>
								<div class="footer-con">
									<a href="<?php bloginfo('url'); ?>/contact">
										<img src="<?php bloginfo('template_url'); ?>/img/common/footer_con.jpg" alt="<?php bloginfo('name'); ?>" />
									</a>										
								</div>
							</div>
						</div><!-- end row -->
						
						<div class="row footer-info2 clearfix">
							<div class="col-35 col-xs-18">
								<ul class="footer-navi">
									<li>
										<a href="<?php bloginfo('url'); ?>"><i class="fa fa-chevron-right"></i>トップページ</a>
									</li>
									<li>
										<a href="<?php bloginfo('url'); ?>/types"><i class="fa fa-chevron-right"></i>タイプを見る</a>
									</li>									
									<li>
										<a href="<?php bloginfo('url'); ?>/draft"><i class="fa fa-chevron-right"></i>データ入稿</a>
									</li>									
								</ul>
							</div><!-- end col -->		
							
							<div class="col-35 col-xs-18">	
								<ul class="footer-navi">
									<li>
										<a href="<?php bloginfo('url'); ?>/original"><i class="fa fa-chevron-right"></i>オリジナルカレンダー</a>
									</li>
									<li>
										<a href="<?php bloginfo('url'); ?>/kabekake"><i class="fa fa-chevron-right"></i>壁掛けカレンダー</a>
									</li>									
									<li>
										<a href="http://kintarou.info/"><i class="fa fa-chevron-right"></i>特殊印刷</a>
									</li>									
								</ul>
							</div><!-- end col -->				

							<div class="col-35 col-xs-18">
								<ul class="footer-navi">
									<li>
										<a href="<?php bloginfo('url'); ?>/about"><i class="fa fa-chevron-right"></i>初めてご利用される方へ</a>
									</li>
									<li>
										<a href="<?php bloginfo('url'); ?>/flow"><i class="fa fa-chevron-right"></i>ご注文の流れ</a>
									</li>
									<li>
										<a href="<?php bloginfo('url'); ?>/faq"><i class="fa fa-chevron-right"></i>よくあるご質問</a>
									</li>									
								</ul>
							</div><!-- end col -->			

							<div class="col-35 col-xs-18">
								<ul class="footer-navi">
									<li>
										<a href="<?php bloginfo('url'); ?>/work"><i class="fa fa-chevron-right"></i>製作実績</a>
									</li>
									<li>
										<a href="<?php bloginfo('url'); ?>/price"><i class="fa fa-chevron-right"></i>料金表</a>
									</li>
									<li>
										<a href="<?php bloginfo('url'); ?>/estimate"><i class="fa fa-chevron-right"></i>お見積り</a>
									</li>
									<li>
										<a href="<?php bloginfo('url'); ?>/factory"><i class="fa fa-chevron-right"></i>工場紹介</a>
									</li>									
								</ul>
							</div><!-- end col -->			

							<div class="col-35 col-xs-18">
								<ul class="footer-navi">
									<li>
										<a href="<?php bloginfo('url'); ?>/company"><i class="fa fa-chevron-right"></i>会社概要</a>
									</li>
									<li>
										<a href="<?php bloginfo('url'); ?>/contact"><i class="fa fa-chevron-right"></i>お問合せ</a>
									</li>
									<li>
										<a href="<?php bloginfo('url'); ?>/privacy"><i class="fa fa-chevron-right"></i>プライバシーポリシー</a>
									</li>									
								</ul>
							</div><!-- end col -->							
						</div><!-- end row -->	
					</div><!-- end container -->        
				</div><!-- /#footer-top  -->
				
				<div id="footer-copyright"><!-- begin footer-copyright -->
					<div class="container clearfix"><!-- begin container -->
						<div class="row clearfix">
							<div class="col-lg-18 col-md-18 col-sm-18 col-xs-18">
								<div class="copyright-title">Copyright©2015 名入れカレンダー製作.com All Rights Reserved.</div>								
							</div>
						</div>
					</div><!-- end container -->  
				</div><!-- end footer-copyright -->
			</footer><!-- end footer -->
                        
        </div><!-- end wrapper -->
        <?php wp_footer(); ?>		
    </body>
</html>