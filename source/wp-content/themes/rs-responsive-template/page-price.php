<?php get_header();?>
<div class="primary-row clearfix">	
	<div class="row clearfix">
        <div class="col-lg-18 col-md-18 col-sm-18 col-xs-18">
			<h2 class="h2-sp-title">
			<img src="<?php bloginfo('template_url'); ?>/img/content/price_top_img.jpg" alt="参考価格例" />
			<span class="h2-sp-text">参考価格例</span>
		</h2>
		</div>		
	</div>	
	<div class="row clearfix">
		<div class="col-lg-18 col-md-18 col-sm-18 col-xs-18 mt20">
			<p>こちらでは下記仕様でカレンダーを製作した場合の口参考価格例をご紹介しております。<br />※あくまで一例の参考の価格になります。<br />その他のお見積りはカレンダーの仕様によって大きく変わってまいります。</p>
		</div>
	</div>
</div><!-- end primary-row -->

<div class="primary-row clearfix">
	<div class="row clearfix">
		<div class="col-lg-18 col-md-18 col-sm-18 col-xs-18">
			<h3 class="price-title">
				<img src="<?php bloginfo('template_url'); ?>/img/content/price_title_top.jpg" alt="参考価格例">
				<div class="price-title-text">卓上カレンダー</div>
			</h3>	
		</div>		
	</div>	

	<div class="row clearfix">
        <div class="col-lg-18 col-md-18 col-sm-18 col-xs-18">
			<table class="price-table1">
				<tbody>
					<tr>
						<th>商品名</th>
						<th>サイズ</th>
						<th>30冊</th>
						<th>50冊</th>
						<th>100冊</th>
						<th>200冊</th>
						<th>300冊</th>
					</tr>
					<tr>
						<td>KP-11<br />卓上レインボーカラー</td>
						<td>プラケース</td>
						<td>325円</td>
						<td>239円</td>
						<td>165円</td>
						<td>154円</td>
						<td>145円</td>						
					</tr>
					<tr>
						<td>KP-21<br />開運招福卓上カレンダー</td>
						<td>プラケース</td>
						<td>340円</td>
						<td>265円</td>
						<td>185円</td>
						<td>170円</td>
						<td>161円</td>						
					</tr>
					<tr>
						<td>GT-201<br />卓上セブンカラー</td>
						<td>三角</td>
						<td>517円</td>
						<td>477円</td>
						<td>326円</td>
						<td>326円</td>
						<td>321円</td>						
					</tr>
					<tr>
						<td>SP-411<br />テレホンナンバー</td>
						<td>卓上</td>
						<td>502円</td>
						<td>325円</td>
						<td>312円</td>
						<td>312円</td>
						<td>301円</td>						
					</tr>
					<tr>
						<td>SG-998<br />マルチスタンド</td>
						<td>PPケース</td>
						<td>420円</td>
						<td>360円</td>
						<td>235円</td>
						<td>235円</td>
						<td>226円</td>						
					</tr>
					<tr>
						<td>SG-971<br />CDスタンド</td>
						<td>プラケース</td>
						<td>516円</td>
						<td>475円</td>
						<td>324円</td>
						<td>324円</td>
						<td>312円</td>						
					</tr>
					<tr>
						<td>GT-206<br />PURE BLUE デスクカレンダー</td>
						<td>三角</td>
						<td>495円</td>
						<td>450円</td>
						<td>305円</td>
						<td>305円</td>
						<td>294円</td>						
					</tr>
					<tr>
						<td>ND-701<br />スケジュールカレンダー</td>
						<td>三角</td>
						<td>475円</td>
						<td>405円</td>
						<td>270円</td>
						<td>270円</td>
						<td>260円</td>						
					</tr>
					<tr>
						<td>SG-951<br />デスクスタンド</td>
						<td>三角</td>
						<td>504円</td>
						<td>504円</td>
						<td>347円</td>
						<td>347円</td>
						<td>334円</td>						
					</tr>
				<tbody>
			</table>
		</div>
	</div>	
</div><!-- end primary-row -->


<div class="primary-row clearfix">
	<div class="row clearfix">
		<div class="col-lg-18 col-md-18 col-sm-18 col-xs-18">
			<h3 class="price-title">
				<img src="<?php bloginfo('template_url'); ?>/img/content/price_title_img.jpg" alt="参考価格例">
				<div class="price-title-text">壁掛けカレンダー（文字月表）</div>
			</h3>	
		</div>		
	</div>	

	<div class="row clearfix">
        <div class="col-lg-18 col-md-18 col-sm-18 col-xs-18">
			<table class="price-table2">
				<tbody>
					<tr>
						<th>商品名</th>
						<th>サイズ</th>
						<th>30冊</th>
						<th>50冊</th>
						<th>100冊</th>
						<th>200冊</th>
						<th>300冊</th>
					</tr>
					<tr>
						<td>GT-305<br />七色の一週間</td>
						<td>四切</td>
						<td>374円</td>
						<td>328円</td>
						<td>218円</td>
						<td>218円</td>
						<td>210円</td>						
					</tr>					
					<tr>
						<td>YG-46<br />匠の美</td>
						<td>四切</td>
						<td>330円</td>
						<td>277円</td>
						<td>178円</td>
						<td>178円</td>
						<td>171円</td>						
					</tr>	
					<tr>
						<td>ND-127<br />4切文字月表</td>
						<td>四切</td>
						<td>351円</td>
						<td>301円</td>
						<td>197円</td>
						<td>197円</td>
						<td>190円</td>						
					</tr>
					<tr>
						<td>YG-40<br />和の文様</td>
						<td>A2</td>
						<td>414円</td>
						<td>368円</td>
						<td>257円</td>
						<td>257円</td>
						<td>248円</td>						
					</tr>
					<tr>
						<td>KA-105<br />スマートメモA2</td>
						<td>A2</td>
						<td>516円</td>
						<td>429円</td>
						<td>305円</td>
						<td>305円</td>
						<td>294円</td>						
					</tr>
					<tr>
						<td>NC-21<br />3色カラーA2</td>
						<td>A2</td>
						<td>510円</td>
						<td>483円</td>
						<td>347円</td>
						<td>347円</td>
						<td>334円</td>						
					</tr>
					<tr>
						<td>GT-206<br />PURE BLUE デスクカレンダー</td>
						<td>B2</td>
						<td>495円</td>
						<td>450円</td>
						<td>305円</td>
						<td>305円</td>
						<td>294円</td>						
					</tr>
					<tr>
						<td>GT-505<br />七色の一週間ジャンボ</td>
						<td>B2</td>
						<td>640円</td>
						<td>600円</td>
						<td>425円</td>
						<td>425円</td>
						<td>410円</td>						
					</tr>
				<tbody>
			</table>
		</div>
	</div>	
</div><!-- end primary-row -->


<div class="primary-row clearfix">
	<div class="row clearfix">
		<div class="col-lg-18 col-md-18 col-sm-18 col-xs-18">
			<h3 class="price-title">
				<img src="<?php bloginfo('template_url'); ?>/img/content/price_title_img.jpg" alt="参考価格例">
				<div class="price-title-text">壁掛けカレンダー（写真月表）</div>
			</h3>	
		</div>		
	</div>	

	<div class="row clearfix">
        <div class="col-lg-18 col-md-18 col-sm-18 col-xs-18">
			<table class="price-table2">
				<tbody>
					<tr>
						<th>商品名</th>
						<th>サイズ</th>
						<th>30冊</th>
						<th>50冊</th>
						<th>100冊</th>
						<th>200冊</th>
						<th>300冊</th>
					</tr>	
					<tr>
						<td>ND-99<br />フラワーカーペット</td>
						<td>四切</td>
						<td>371円</td>
						<td>361円</td>
						<td>254円</td>
						<td>254円</td>
						<td>245円</td>						
					</tr>			
					<tr>
						<td>ND-119<br />パシフィックオーシャン</td>
						<td>A2</td>
						<td>426円</td>
						<td>413円</td>
						<td>296円</td>
						<td>296円</td>
						<td>286円</td>						
					</tr>		
					<tr>
						<td>YG-5<br />ふるさと</td>
						<td>四切</td>
						<td>284円</td>
						<td>347円</td>
						<td>173円</td>
						<td>173円</td>
						<td>167円</td>						
					</tr>
					<tr>
						<td>ND107<br />ドッグファミリー</td>
						<td>四切</td>
						<td>441円</td>
						<td>409円</td>
						<td>281円</td>
						<td>325円</td>
						<td>282円</td>						
					</tr>
					<tr>
						<td>SG-163<br />マイスウィーティーキャット</td>
						<td>四切</td>
						<td>297円</td>
						<td>273円</td>
						<td>186円</td>
						<td>186円</td>
						<td>179円</td>						
					</tr>
					<tr>
						<td>SC-32<br />ラブリーペット</td>
						<td>四切</td>
						<td>407円</td>
						<td>404円</td>
						<td>288円</td>
						<td>288円</td>
						<td>278円</td>						
					</tr>
				<tbody>
			</table>
		</div>
	</div>	
</div><!-- end primary-row -->

<div class="primary-row clearfix">
	<div class="row clearfix">
		<div class="col-lg-18 col-md-18 col-sm-18 col-xs-18">
			<h3 class="price-title">
				<img src="<?php bloginfo('template_url'); ?>/img/content/price_title_img.jpg" alt="参考価格例">
				<div class="price-title-text">壁掛けカレンダー（絵柄）</div>
			</h3>	
		</div>		
	</div>	

	<div class="row clearfix">
        <div class="col-lg-18 col-md-18 col-sm-18 col-xs-18">
			<table class="price-table2">
				<tbody>
					<tr>
						<th>商品名</th>
						<th>サイズ</th>
						<th>30冊</th>
						<th>50冊</th>
						<th>100冊</th>
						<th>200冊</th>
						<th>300冊</th>
					</tr>	
					<tr>
						<td>HB-17<br />万葉集 花巡り</td>
						<td>四切</td>
						<td>462円</td>
						<td>435円</td>
						<td>312円</td>
						<td>312円</td>
						<td>301円</td>						
					</tr>								
					<tr>
						<td>NB-731<br />小さなメルヘン</td>
						<td>四切</td>
						<td>200円</td>
						<td>120円</td>
						<td>67円</td>
						<td>67円</td>
						<td>64円</td>						
					</tr>
					<tr>
						<td>SG-281<br />詩情・四季メモリー</td>
						<td>四切</td>
						<td>462円</td>
						<td>435円</td>
						<td>312円</td>
						<td>312円</td>
						<td>301円</td>						
					</tr>
					<tr>
						<td>NA-103<br />ネイチャーフィールド</td>
						<td>A2</td>
						<td>579円</td>
						<td>566円</td>
						<td>411円</td>
						<td>411円</td>
						<td>396円</td>						
					</tr>
					<tr>
						<td>MS-418<br />元気が出るかれんだー</td>
						<td>四切</td>
						<td>200円</td>
						<td>120円</td>
						<td>120円</td>
						<td>67円</td>
						<td>64円</td>						
					</tr>
					<tr>
						<td>NC-10<br />夢ふくらむ</td>
						<td>四切</td>
						<td>513円</td>
						<td>496円</td>
						<td>424円</td>
						<td>424円</td>
						<td>409円</td>						
					</tr>
					<tr>
						<td>TD-841<br />花あそび</td>
						<td>四切</td>
						<td>582円</td>
						<td>496円</td>
						<td>424円</td>
						<td>424円</td>
						<td>409円</td>						
					</tr>
					<tr>
						<td>ND-118<br />にほん歳時期</td>
						<td>四切</td>
						<td>452円</td>
						<td>422円</td>
						<td>291円</td>
						<td>291円</td>
						<td>280円</td>						
					</tr>
					<tr>
						<td>NA-147<br />二十四筋気</td>
						<td>四切</td>
						<td>473円</td>
						<td>447円</td>
						<td>322円</td>
						<td>322円</td>
						<td>310円</td>						
					</tr>
				<tbody>
			</table>
		</div>
	</div>	
</div><!-- end primary-row -->

<?php get_template_part('part','flow'); ?>  

<?php get_footer();?>