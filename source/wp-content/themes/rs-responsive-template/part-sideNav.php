<div class="sideNav">
	<h2 class="side-navi-title">
		<img src="<?php bloginfo('template_url'); ?>/img/common/side_navi_title.png" alt="<?php bloginfo('name'); ?>"/>
		<span class="side-title">ご利用ガイド</span>
	</h2>

    <ul>
        <li><a href="<?php bloginfo('url'); ?>/about">初めてご利用される方へ<i class="fa fa-chevron-right"></i></a></li>
        <li><a href="<?php bloginfo('url'); ?>/flow">ご注文の流れ<i class="fa fa-chevron-right"></i></a></li>
        <li><a href="<?php bloginfo('url'); ?>/price">料金表<i class="fa fa-chevron-right"></i></a></li>
        <li><a href="<?php bloginfo('url'); ?>/faq">よくあるご質問<i class="fa fa-chevron-right"></i></a></li>
        <li><a href="<?php bloginfo('url'); ?>/privacy">プライバシーポリシー<i class="fa fa-chevron-right"></i></a></li>
        <li><a href="<?php bloginfo('url'); ?>/company">会社概要<i class="fa fa-chevron-right"></i></a></li>
        <li><a href="<?php bloginfo('url'); ?>/contact">お問合せ<i class="fa fa-chevron-right"></i></a></li>
    </ul>
</div>