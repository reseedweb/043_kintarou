<?php get_header();?>
<div class="primary-row clearfix">	
	<div class="row clearfix">
        <div class="col-lg-18 col-md-18 col-sm-18 col-xs-18">
			<h2 class="h2-sp-title">
			<img src="<?php bloginfo('template_url'); ?>/img/content/faq_top_img.jpg" alt="よくあるご質問" />
			<span class="h2-sp-text">よくあるご質問</span>
		</h2>
		</div>		
	</div>	
	<div class="row clearfix">
		<div class="col-lg-18 col-md-18 col-sm-18 col-xs-18 mt20">
			<p>名入れカレンダーの制作に関してよくあるご質問を掲載いたします。<br />こちらに掲載されている以外のご質問は直接お問い合わせください。</p>
		</div>
	</div>
</div><!-- end primary-row -->


<div class="primary-row clearfix">	
	<div class="row clearfix">
        <div class="col-lg-18 col-md-18 col-sm-18 col-xs-18">
			<h3 class="title-sub">
				<img src="<?php bloginfo('template_url'); ?>/img/content/title_sub.png" alt="よくあるご質問" />
				<div class="title-sub-text">カレンダーについて</div>
			</h3>
		</div>
	</div>
	<div class="row clearfix">	
		<div class="col-lg-18 col-md-18 col-sm-18 col-xs-18">
			<div class="faq-q clearfix">
				<img src="<?php bloginfo('template_url'); ?>/img/content/faq_content_question.jpg" alt="よくあるご質問" />
				<span class="faq-q-text">Q. エコタイプとは何ですか？</span>
			</div>
			<div class="faq-a mb30 clearfix">
				<div class="faq-a-img"><img src="<?php bloginfo('template_url'); ?>/img/content/faq_content_answer.jpg" alt="よくあるご質問" /></div>
				<div class="faq-a-text">ペット樹脂を材質として利用しており、エコロジー・リサイクルの両方に対応しているケースです。<br />やわらかい素材なので、割れに強いという利点があります。</div>
			</div>
		</div>
	</div>

	<div class="row clearfix">	
		<div class="col-lg-18 col-md-18 col-sm-18 col-xs-18">
			<div class="faq-q clearfix">
				<img src="<?php bloginfo('template_url'); ?>/img/content/faq_content_question.jpg" alt="よくあるご質問" />
				<span class="faq-q-text">Q. ハードタイプとは何ですか？</span>
			</div>
			<div class="faq-a mb30 clearfix">
				<div class="faq-a-img"><img src="<?php bloginfo('template_url'); ?>/img/content/faq_content_answer.jpg" alt="よくあるご質問" /></div>
				<div class="faq-a-text">ケース自体は硬い材質なので、落としてしまうと割れる恐れがあります。</div>
			</div>
		</div>
	</div>

	<div class="row clearfix">	
		<div class="col-lg-18 col-md-18 col-sm-18 col-xs-18">
			<div class="faq-q clearfix">
				<img src="<?php bloginfo('template_url'); ?>/img/content/faq_content_question.jpg" alt="よくあるご質問" />
				<span class="faq-q-ltext">Q. イメージ画像を見てから決めたいのですが可能ですか？</span>
			</div>
			<div class="faq-a mb30 clearfix">
				<div class="faq-a-img"><img src="<?php bloginfo('template_url'); ?>/img/content/faq_content_answer.jpg" alt="よくあるご質問" /></div>
				<div class="faq-a-text">可能です。ご注文後にご確認いただくか、先に見てから決めたい場合は当社までお問合せください。</div>
			</div>
		</div>
	</div>

	<div class="row clearfix">	
		<div class="col-lg-18 col-md-18 col-sm-18 col-xs-18">
			<div class="faq-q clearfix">
				<img src="<?php bloginfo('template_url'); ?>/img/content/faq_content_question.jpg" alt="よくあるご質問" />
				<span class="faq-q-ltext">Q. 14枚組み、15枚組みのカレンダーの制作もできますか？</span>
			</div>
			<div class="faq-a mb30 clearfix">
				<div class="faq-a-img"><img src="<?php bloginfo('template_url'); ?>/img/content/faq_content_answer.jpg" alt="よくあるご質問" /></div>
				<div class="faq-a-text">可能です。ただ、こちらはオプションとなりますので別途お見積もりをさせていただきます。</div>
			</div>
		</div>
	</div>

	<div class="row clearfix">	
		<div class="col-lg-18 col-md-18 col-sm-18 col-xs-18">
			<div class="faq-q clearfix">
				<img src="<?php bloginfo('template_url'); ?>/img/content/faq_content_question.jpg" alt="よくあるご質問" />
				<span class="faq-q-lltext">Q. 持っている画像と無料イメージ画像を組み合わせて作ることは可能ですか？</span>
			</div>
			<div class="faq-a mb30 clearfix">
				<div class="faq-a-img"><img src="<?php bloginfo('template_url'); ?>/img/content/faq_content_answer.jpg" alt="よくあるご質問" /></div>
				<div class="faq-a-text">可能です。無料イメージ画像はご自由に組み合わせください。</div>
			</div>
		</div>
	</div>

	<div class="row clearfix">	
		<div class="col-lg-18 col-md-18 col-sm-18 col-xs-18">
			<div class="faq-q clearfix">
				<img src="<?php bloginfo('template_url'); ?>/img/content/faq_content_question.jpg" alt="よくあるご質問" />
				<span class="faq-q-ltext">Q. 4月から1年間分のカレンダーはできますか？</span>
			</div>
			<div class="faq-a mb30 clearfix">
				<div class="faq-a-img"><img src="<?php bloginfo('template_url'); ?>/img/content/faq_content_answer.jpg" alt="よくあるご質問" /></div>
				<div class="faq-a-text">可能です。（今年の12月～来年の12月まで）や（4月スタートのスクールカレンダー）などご要望に合わせてスタート月を変えて制作できます。</div>
			</div>
		</div>
	</div>

	<div class="row clearfix">	
		<div class="col-lg-18 col-md-18 col-sm-18 col-xs-18">
			<div class="faq-q clearfix">
				<img src="<?php bloginfo('template_url'); ?>/img/content/faq_content_question.jpg" alt="よくあるご質問" />
				<span class="faq-q-lltext">Q. 今年の10月から来年の12月までの14ヶ月分のカレンダーはできますか？</span>
			</div>
			<div class="faq-a mb30 clearfix">
				<div class="faq-a-img"><img src="<?php bloginfo('template_url'); ?>/img/content/faq_content_answer.jpg" alt="よくあるご質問" /></div>
				<div class="faq-a-text">可能です。枚数に制限はなく、ケースに入れば何枚でも対応可能です。<br />来年の1月～再来年の3月までといった15枚組み（表紙を入れると16枚組み）カレンダーなども対応可能です。</div>
			</div>
		</div>
	</div>

	<div class="row clearfix">	
		<div class="col-lg-18 col-md-18 col-sm-18 col-xs-18">
			<div class="faq-q clearfix">
				<img src="<?php bloginfo('template_url'); ?>/img/content/faq_content_question.jpg" alt="よくあるご質問" />
				<span class="faq-q-lltext">Q. すべてオリジナルのデザインのカレンダーを作りたいのですが？</span>
			</div>
			<div class="faq-a clearfix">
				<div class="faq-a-img"><img src="<?php bloginfo('template_url'); ?>/img/content/faq_content_answer.jpg" alt="よくあるご質問" /></div>
				<div class="faq-a-text">可能です。ご希望を聞かせて頂いた上で、お客様だけのオリジナルデザインを作成いたします。<br />納期や料金などはお問い合わせください。</div>
			</div>
		</div>
	</div>
</div><!-- end primary-row -->


<div class="primary-row clearfix">	
	<div class="row clearfix">
        <div class="col-lg-18 col-md-18 col-sm-18 col-xs-18">
			<h3 class="title-sub">
				<img src="<?php bloginfo('template_url'); ?>/img/content/title_sub.png" alt="印刷について" />
				<div class="title-sub-text">印刷について</div>
			</h3>
		</div>
	</div>
	<div class="row clearfix">	
		<div class="col-lg-18 col-md-18 col-sm-18 col-xs-18">
			<div class="faq-q clearfix">
				<img src="<?php bloginfo('template_url'); ?>/img/content/faq_content_question.jpg" alt="印刷について" />
				<span class="faq-q-text">Q. オンデマンド印刷とは何ですか？</span>
			</div>
			<div class="faq-a mb30 clearfix">
				<div class="faq-a-img"><img src="<?php bloginfo('template_url'); ?>/img/content/faq_content_answer.jpg" alt="印刷について" /></div>
				<div class="faq-a-text">デジタル印刷機による印刷のことで、高性能のデジタルプリンターにて行う印刷のことを指します。<br />他の印刷と比べ低コストで綺麗ですが、通常の印刷と違いベタ塗りやグラデーションに弱いというデメリットがあります。</div>
			</div>
		</div>
	</div>

	<div class="row clearfix">	
		<div class="col-lg-18 col-md-18 col-sm-18 col-xs-18">
			<div class="faq-q clearfix">
				<img src="<?php bloginfo('template_url'); ?>/img/content/faq_content_question.jpg" alt="印刷について" />
				<span class="faq-q-lltext">Q. 300部/500部/200部別々で合計1000部欲しいのですが可能ですか？</span>
			</div>
			<div class="faq-a mb30 clearfix">
				<div class="faq-a-img"><img src="<?php bloginfo('template_url'); ?>/img/content/faq_content_answer.jpg" alt="印刷について" /></div>
				<div class="faq-a-text">可能です。その場合は、300部＋500部＋200部の合計料金となります。</div>
			</div>
		</div>
	</div>

	<div class="row clearfix">	
		<div class="col-lg-18 col-md-18 col-sm-18 col-xs-18">
			<div class="faq-q clearfix">
				<img src="<?php bloginfo('template_url'); ?>/img/content/faq_content_question.jpg" alt="印刷について" />
				<span class="faq-q-text">Q. 増刷は可能ですか？</span>
			</div>
			<div class="faq-a mb30 clearfix">
				<div class="faq-a-img"><img src="<?php bloginfo('template_url'); ?>/img/content/faq_content_answer.jpg" alt="印刷について" /></div>
				<div class="faq-a-text">可能です。増刷の場合は、データ入稿と同じ価格にてご対応させていただいております。注意点として、500部以上印刷して100部後ほど追加する場合にCTP印刷からオンデマンド印刷に変わってしまいます。仕上がりの感じが若干変わりますのでご注意ください。</div>
			</div>
		</div>
	</div>

</div><!-- end primary-row -->


<div class="primary-row clearfix">	
	<div class="row clearfix">
        <div class="col-lg-18 col-md-18 col-sm-18 col-xs-18">
			<h3 class="title-sub">
				<img src="<?php bloginfo('template_url'); ?>/img/content/title_sub.png" alt="配送・納期について" />
				<div class="title-sub-text">配送・納期について</div>
			</h3>
		</div>
	</div>

	<div class="row clearfix">	
		<div class="col-lg-18 col-md-18 col-sm-18 col-xs-18">
			<div class="faq-q clearfix">
				<img src="<?php bloginfo('template_url'); ?>/img/content/faq_content_question.jpg" alt="配送・納期について" />
				<span class="faq-q-ltext">Q. 納品日の指定・時間の指定はできますか？</span>
			</div>
			<div class="faq-a mb30 clearfix">
				<div class="faq-a-img"><img src="<?php bloginfo('template_url'); ?>/img/content/faq_content_answer.jpg" alt="配送・納期について" /></div>
				<div class="faq-a-text">可能です。ただ、11月～1月に関しては、宅急便が大変混み合う時期のため、お時間のお約束をすることが出来かねます。ご希望は頂戴いたします。</div>
			</div>
		</div>
	</div>

	<div class="row clearfix">	
		<div class="col-lg-18 col-md-18 col-sm-18 col-xs-18">
			<div class="faq-q clearfix">
				<img src="<?php bloginfo('template_url'); ?>/img/content/faq_content_question.jpg" alt="配送・納期について" />
				<span class="faq-q-ltext">Q. 納品場所が複数ですが、対応できますか？</span>
			</div>
			<div class="faq-a mb30 clearfix">
				<div class="faq-a-img"><img src="<?php bloginfo('template_url'); ?>/img/content/faq_content_answer.jpg" alt="配送・納期について" /></div>
				<div class="faq-a-text">可能です。複数の場合は別途送料がかかります。 送料は1箇所のみ無料になりますので、無料分は一番送料のかかる場所への送料を無料とさせていただきます。<br />
					複数の場所への送料は個数と場所により価格が異なりますので、お見積もりを希望の方はご連絡ください。<br />
					（目安）カレンダー200個で1個口…2,000円程度
				</div>
			</div>
		</div>
	</div>

	<div class="row clearfix">	
		<div class="col-lg-18 col-md-18 col-sm-18 col-xs-18">
			<div class="faq-q clearfix">
				<img src="<?php bloginfo('template_url'); ?>/img/content/faq_content_question.jpg" alt="配送・納期について" />
				<span class="faq-q-text">Q. 1つずつの個別発送は可能でしょうか？</span>
			</div>
			<div class="faq-a mb30 clearfix">
				<div class="faq-a-img"><img src="<?php bloginfo('template_url'); ?>/img/content/faq_content_answer.jpg" alt="配送・納期について" /></div>
				<div class="faq-a-text">大変申し訳ないのですが、カレンダー1個ずつの個別発送はお断りしております。</div>
			</div>
		</div>
	</div>

	<div class="row clearfix">	
		<div class="col-lg-18 col-md-18 col-sm-18 col-xs-18">
			<div class="faq-q clearfix">
				<img src="<?php bloginfo('template_url'); ?>/img/content/faq_content_question.jpg" alt="配送・納期について" />
				<span class="faq-q-text">Q. 打ち合わせは出来ますか？</span>
			</div>
			<div class="faq-a clearfix">
				<div class="faq-a-img"><img src="<?php bloginfo('template_url'); ?>/img/content/faq_content_answer.jpg" alt="配送・納期について" /></div>
				<div class="faq-a-text">可能です。当店にて打合せをすることができます。</div>
			</div>
		</div>
	</div>
</div><!-- end primary-row -->

<?php get_template_part('part','flow'); ?>  

<?php get_footer();?>