			<section id="slider-banner"><!-- begin slider -->				
				<ul class="bxslider">
					<li>
						<img src="<?php bloginfo('template_url'); ?>/img/top/slider_main_img1.jpg" alt="top" />										
					</li>
					<li>
						<img src="<?php bloginfo('template_url'); ?>/img/top/slider_main_img2.jpg" alt="top" />					
					</li>
					<li>
						<img src="<?php bloginfo('template_url'); ?>/img/top/slider_main_img3.jpg" alt="top" />					
					</li>				
				</ul>

				<div id="bx-pager">
					<div class="container"><!-- begin container -->
						<div class="row clearfix"><!-- begin row -->
							<div class="col-lg-6 col-md-6 col-sm-6 col-xs-6"><!-- begin col -->     
								<a data-slide-index="0" href="<?php bloginfo('url'); ?>">
									<img src="<?php bloginfo('template_url'); ?>/img/top/slider_thumb_img1.jpg" alt="top" />					
								</a>
							</div>							
							<div class="col-lg-6 col-md-6 col-sm-6 col-xs-6"><!-- begin col -->     
								<a data-slide-index="1" href="<?php bloginfo('url'); ?>">
									<img src="<?php bloginfo('template_url'); ?>/img/top/slider_thumb_img2.jpg" alt="top" />			
								</a>
							</div>
							<div class="col-lg-6 col-md-6 col-sm-6 col-xs-6"><!-- begin col -->     
								<a data-slide-index="2" href="<?php bloginfo('url'); ?>">
									<img src="<?php bloginfo('template_url'); ?>/img/top/slider_thumb_img3.jpg" alt="top" />					
								</a>
							</div>
						</div>
					</div>																		
				</div>
			</section><!-- end slider -->