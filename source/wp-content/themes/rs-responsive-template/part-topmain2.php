			<section id="top-main2"><!-- begin top-main2 -->
				<div class="container"><!-- begin container -->
                    <div class="row clearfix">
                    	<div class="col-lg-18 col-md-18 col-sm-18 col-xs-18">
                    		<h2 class="top-main2-title">
                    			<img src="<?php bloginfo('template_url'); ?>/img/top/top_main2_title.png" alt="<?php bloginfo('name'); ?>" />
	                    		<div class="tmain2-ttext">選べる壁掛けカレンダー<span class="tmain2-tclr1">3</span><span class="tmain2-tclr2">タイプ</span></div>
	                    	</h2>
                    	</div>                    	
                    </div><!-- end row -->
					<?php	
					$kabekake_posts = get_posts( 	array(
						'post_type'=> 'kabekake',
						'posts_per_page' => 4,
						'paged' => get_query_var('paged'),
					));
					?>
					<?php $i = 0;?>
					<?php foreach($kabekake_posts as $kabekake_post):?>
					<?php $i++; ?>
					<?php if($i%4 == 1) : ?>			
					<div class="row clearfix" id="top-main2-top">        		
					<?php endif; ?>						
						<div class="col-tlg4 col-tsm4 col-txs9 xs-mt30 clearfix">				
                    		<div class="top-main2-content2 clearfix">
                    			<div class="top-mcontent22-icon"></div>
                    			<div class="top-mcontent22-info clearfix">
									<div class="top-line clearfix">
										<p class="top-mcontent22-img"><?php echo get_the_post_thumbnail($kabekake_post->ID,'medium'); ?></p>
										<h3 class="top-mc22-title"><?php @the_terms($kabekake_post->ID, 'cat-kabekake'); ?></h3>
										<p class="top-mc22-text1"><a href="<?php echo get_the_permalink($kabekake_post->ID); ?>"><?php echo $kabekake_post->post_title; ?></a></p>
									</div>                    				
                    				<div class="top-mc22-text2 clearfix">
                    					<div class="top-mc22-size1"><span class="top-mc22-size">size</span><?php echo get_field('text2', $kabekake_post->ID); ?></div>
                    					<div class="top-mc22-size2"><span class="top-mc22-size">price</span><span class="top-mc22-symbol1">@</span><?php echo get_field('text3', $kabekake_post->ID); ?><span class="top-mc22-symbol2">円</span></div>
                    				</div>                    				
                    			</div>
                    		</div>
                    	</div>       
					<?php if($i%4 == 0 || $i == count($kabekake_posts) ) : ?>				
					</div><!-- end row -->
					<?php endif; ?>
					<?php endforeach; ?>                    
  				</div><!-- end container -->
			</section><!-- end top-main2 -->