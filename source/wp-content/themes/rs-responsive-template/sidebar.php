<div class="sidebar-row clearfix"><!-- begin sidebar-row -->
    <div class="side-con">
        <img src="<?php bloginfo('template_url'); ?>/img/common/side_con_bg.jpg" alt="<?php bloginfo('name'); ?>"/>
        <p class="side-con-text">カレンダーに関するご相談・お問合せはコチラ</p>
        <p class="side-con-btn">            
            <a href="<?php bloginfo('url'); ?>/contact">
                   <img src="<?php bloginfo('template_url'); ?>/img/common/side_con_btn.jpg" alt="<?php bloginfo('name'); ?>"/>
            </a>                
        </p>                        
    </div><!-- ./sideBanner_contact_bg -->   
</div><!-- end sidebar-row -->

<div class="sidebar-row clearfix"><!-- begin sidebar-row -->    
    <?php get_template_part('part','sideMenu'); ?>
</div><!-- end sidebar-row -->

<div class="sidebar-row clearfix"><!-- begin sidebar-row -->
    <p>
        <a href="<?php bloginfo('url'); ?>/draft">
            <img src="<?php bloginfo('template_url'); ?>/img/common/side_draft.jpg" alt="<?php bloginfo('name'); ?>"/>
        </a>
    </p>
    <p class="side-info-text">詳しい入稿方法はこちらでご紹介！</p>
</div><!-- end sidebar-row -->

<div class="sidebar-row clearfix"><!-- begin sidebar-row -->
    <p>
        <a href="<?php bloginfo('url'); ?>/work">
            <img src="<?php bloginfo('template_url'); ?>/img/common/side_work.jpg" alt="<?php bloginfo('name'); ?>"/>
        </a>
    </p>
    <p class="side-info-text">弊社とお取引させていただいたお客様をご紹介！</p>
</div><!-- end sidebar-row -->

<div class="sidebar-row clearfix"><!-- begin sidebar-row -->
    <p>
        <a href="http://kintarou.info/">
            <img src="<?php bloginfo('template_url'); ?>/img/common/side_link.jpg" alt="<?php bloginfo('name'); ?>"/>
        </a>
    </p>
    <p class="side-info-text">シルク印刷・パッド印刷など、特殊印刷はお任せください！</p>
</div><!-- end sidebar-row -->

<div class="mt30 clearfix"><!-- begin sidebar-row -->
    <?php get_template_part('part','sideNav'); ?>
</div><!-- end sidebar-row -->                       