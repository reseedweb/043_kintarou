			<section id="top-main1"><!-- begin top-main1 -->
				<div class="container"><!-- begin container -->
                    <div class="row clearfix">
                    	<div class="col-lg-18 col-md-18 col-sm-18 col-xs-18">
                    		<h2 class="top-main1-title">
                    			<img src="<?php bloginfo('template_url'); ?>/img/top/top_main1_title.png" alt="<?php bloginfo('name'); ?>" />
	                    		<div class="tmain1-ttext">選べる卓上カレンダー<span class="tmain1-tclr1">7</span><span class="tmain1-tclr2">タイプ</span></div>
	                    	</h2>
                    	</div>                    	
                    </div><!-- end row -->
					<div id="top-main1-info">
						 <?php	
							$type_posts = get_posts( 	array(
								'post_type'=> 'types',
								'posts_per_page' => 4,
								'paged' => get_query_var('paged'),
							));
						?>
					<?php $i = 0;?>
					<?php foreach($type_posts as $type_post):?>
					<?php $i++; ?>
					<?php if($i%4 == 1) : ?>			
					<div class="row top-main1-box-bg clearfix">
						<?php endif; ?>				                    
							<div class="col-dlg4 col-dsm4 col-dxs9 xs-arrange clearfix">
								<div class="top-main1-box clearfix"> 
									<div class="top-mbox-icon"></div>               			                    			
									<div class="top-mbox-info clearfix">
										<div class="top-line clearfix">
											<p class="top-mbox-img"><?php echo get_the_post_thumbnail($type_post->ID,'medium'); ?></p>
											<h3 class="top-mbox-title"><?php @the_terms($type_post->ID, 'cat-types'); ?></h3>
											<p class="top-mbox-text1"><a href="<?php echo get_the_permalink($type_post->ID); ?>"><?php echo $type_post->post_title; ?></a></p>
										</div>										
										<div class="top-mbox-text2 clearfix">
											<div class="top-mbox-size1"><span class="top-mbox-size">size</span><?php echo get_field('text2', $type_post->ID); ?></div>
											<div class="top-mbox-size2"><span class="top-mbox-size">price</span><span class="top-mbox-symbol1">@</span><?php echo get_field('text3', $type_post->ID); ?><span class="top-mbox-symbol2">円</span></div>
										</div>                    				
									</div>
								</div>
							</div>                     	               		
						<?php if($i%4 == 0 || $i == count($type_posts) ) : ?>				
						</div><!-- end row -->
						<?php endif; ?>
						<?php endforeach; ?>
					</div>
                     
                </div><!-- end container -->
			</section><!-- end top-main1 -->