/* htabs */
$.fn.htabs = function() {
    var selector = this;
    hash = window.location.hash;
    
    this.each(function() {
            var obj = $(this); 
            $(obj.attr('href')).hide();

            $(obj).click(function() {
                $(selector).removeClass('selected');

                $(selector).each(function(i, element) {
                        $($(element).attr('href')).hide();
                });

                $(this).addClass('selected');

                $($(this).attr('href')).show();                

                return false;
            });
    });    

    $(this).first().click();
    
    $(this).show();
    /*
    if(hash == '')
    {
        $(this).first().click();
    }
    else
    {
        $(hash).show();
        $(hash).parents().show();
    }*/
};
$.fn.ajaxtabs = function(options){
    var default_options = {
        content_wrapper : '#ajax-content-wrapper',
        data : {}
    }; 
    
    options = $.extend({},default_options, options);

    var selector = this;
    

    this.each(function() {
            var obj = $(this);            

            $(obj).click(function() {
                $(selector).removeClass('selected');

                $(this).addClass('selected');                
                $(options.content_wrapper).load($(this).attr('href'), options.data, function(){
                    
                });                

                return false;
            });
    });
    $(this).first().click();
    
    $(this).show();
}

var isMobile = {
    Android: function() {
        return navigator.userAgent.match(/Android/i);
    },
    BlackBerry: function() {
        return navigator.userAgent.match(/BlackBerry/i);
    },
    iOS: function() {
        return navigator.userAgent.match(/iPhone|iPad|iPod/i);
    },
    Opera: function() {
        return navigator.userAgent.match(/Opera Mini/i);
    },
    Windows: function() {
        return navigator.userAgent.match(/IEMobile/i);
    },
    any: function() {
        return (isMobile.Android() || isMobile.BlackBerry() || isMobile.iOS() || isMobile.Opera() || isMobile.Windows());
    }
};

function is_mobile(){
    var screen_type = $('#screen_type').css('content');       
    if(
        screen_type === 'lg' || screen_type === 'md' || screen_type === 'sm' // chrome
        ||
        screen_type === '"lg"' || screen_type === '"md"' || screen_type === '"sm"' //firefox
        ||
        screen_type === undefined
    ) return false;
    return true;    
}

function get_screen_type(){
    var screen_type = $('#screen_type').css('content');    
    switch(screen_type){
        case undefined :        
            return 'ie';        
        case 'lg':
            return 'lg';        
        case '"md"':
        case 'md':
            return 'md';
        case 'sm':
        case '"sm"':
            return 'sm';
        case 'xs':
        case '"xs"':
            return 'xs';                     
        default:
            return 'lg';
    }
}

/*----- js top-navi -----*/
$(window).on('load resize', function(){
	var w = $(window).width();
	var x = 500;
	var sp_x = 480;
	$("#top-flow, #content").waypoint({
		handler: function(direction) {
			if (direction == 'down' && w > x) {
				$("#top-header-info").slideUp();
			}else if(direction == 'up' && w > x){
				$("#top-header-info").slideDown();
			}
		}
	});	
	$("#content").waypoint({
		handler: function(direction) {
			if (direction == 'down' && w > x) {
				$("#top-header-info").slideUp();
			}else if(direction == 'up' && w > x){
				$("#top-header-info").slideDown();
			}
		}
	});	
});

$(document).ready(function() {	
	// bxslider
	jQuery('.bxslider').bxSlider({					
        pagerCustom: '#bx-pager',                
		controls: true,
		auto: true,
		autoControls: false,
		stop: true
    });  
	
	$('#wrapper').addClass(get_screen_type());
    $( window ).resize(function(){
        $('#wrapper').attr('class', get_screen_type());
    });
	
	/*----- js top-navi current -----*/
	$('a').each(function() {
		if ($(this).prop('href') == window.location.href) {
		  $(this).addClass('link-current');
		}
	});
	
	/*----- js topnavi current -----*/
	$(".topnavi li a").each(function(){
        if ($(this).attr("href") == window.location.href){
			$(this).addClass("topnavi-current");
        }
	});
	
	/*----- js footer-info2-current current -----*/
	$(".footer-info2 li a").each(function(){
        if ($(this).attr("href") == window.location.href){
			$(this).addClass("footer-info2-current");
        }
	});	
	
	/*----- js sideMenu current -----*/
	$(".sideMenu ul li a").each(function(){
        if ($(this).attr("href") == window.location.href){
			$(this).addClass("sideMenu-current");
        }
	});

	/*----- js sideNav current -----*/
	$(".sideNav ul li a").each(function(){
        if ($(this).attr("href") == window.location.href){
			$(this).addClass("sideNav-current");
        }
	});

	/*----- js type-tabs-detail current -----*/
	$(".type-tabs-detail li a").each(function(){
       	if (this.href == window.location) {
		  $(this).addClass('typelinks-current');
		}
	});
	
	/*----- js kabekake-tabs-detail current -----*/
	$(".kabekake-tabs-detail li a").each(function(){
       	if (this.href == window.location) {
		  $(this).addClass('kabekakedlink-current');
		}
	});
	
	/*----- js contact -----*/
	$('#zip').change(function(){					
		//AjaxZip3.JSONDATA = "https://ajaxzip3.googlecode.com/svn/trunk/ajaxzip3/zipdata";
		AjaxZip3.zip2addr(this,'','pref','addr1','addr2');
	});
	
	/*----- js estimate -----*/
	$('.tab-list a').htabs();
		
	/*----- js top-navi2 for sp -----*/
	$('#right-menu').sidr({
		name: 'sidr-right',
	    side: 'right',
		source : '#navi-sp'
	});
	
	$(".sidr ul").on('click', 'li', function(){	
	if ($(this).children("ul").css('display') == 'none') {
		$(".sidr ul li ul").slideUp();
        $(this).children('ul').slideDown();
	}
	else{
	   $(this).children('ul').slideUp();
	}
    });
	
	/*----- js effect change object -----*/
	$(window).scroll( function(){						
		$('.wpb_animate_when_almost_visible').each( function(i){								
			var bottom_of_object = $(this).offset().top + $(this).outerHeight();
			var bottom_of_window = $(window).scrollTop() + $(window).height();								
			if( bottom_of_window > bottom_of_object ){           
				$(this).addClass('wpb_start_animation');
			}								
		}); 						
	});	
	
	
	//$(".top-line").heightLine();
	
	var sp_img = $('#slider-banner').children('img'); 
	$(window).setBreakpoints({
		distinct: true,
		breakpoints: [ 1, 640 ]
	});
	$(window).bind('enterBreakpoint640',function() {
		sp_img.each(function() {
		  $(this).attr('src', $(this).attr('src').replace('_sp', '_pc'));
		});
	});
	$(window).bind('enterBreakpoint1',function() {
		sp_img.each(function() {
		  $(this).attr('src', $(this).attr('src').replace('_pc', '_sp'));
		});
	});  
});

jQuery(function(){
	$(this).tgImageChange({
	selectorThumb : '.thumb', // ←★サムネイル画像セレクタ(ドットは必須)
	fadeOutTime : 50, // ←★カーソルON時のアニメーション時間
	fadeInTime : 200, // ←★カーソルOFF時のアニメーション時間
	thumbCssBorder : '2px solid #ff5a71', // ←★カーソルON時のサムネイル枠CSS
	});
}); 