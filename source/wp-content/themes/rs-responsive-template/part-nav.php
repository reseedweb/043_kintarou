			<nav>
				<div id="menu-toggle"><!-- begin menu-toggle -->
					<div class="globalNavi-bg"><!-- begin globalNavi-bg -->
						<div class="container"><!-- begin container -->														
								<div class="globalNavi row clearfix">
									<ul>
										<li><a href="<?php bloginfo('url'); ?>"><i class="fa fa-home"></i>トップページ</a></li>										
										<li><a href="<?php bloginfo('url'); ?>/types"><i class="fa fa-calendar-o"></i>卓上カレンダー</a></li>
										<li><a href="<?php bloginfo('url'); ?>/kabekake"><i class="fa fa-calendar"></i>壁掛けカレンダー</a></li>
										<li><a href="<?php bloginfo('url'); ?>/price"><i class="fa fa-jpy"></i>料金表</a></li>
										<li><a href="<?php bloginfo('url'); ?>/draft"><i class="fa fa-folder-open"></i>データ入稿</a></li>
										<li><a href="<?php bloginfo('url'); ?>/estimate"><i class="fa fa-calculator"></i>お見積り</a></li>
									</ul>
								</div>                            							
						</div><!-- end container -->
					</div><!-- end globalNavi-bg -->					      
				</div><!-- end menu-toggle -->
			</nav><!-- end nav -->