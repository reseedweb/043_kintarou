<?php get_header();?>
<div class="primary-row clearfix">	
	<div class="row clearfix">
        <div class="col-lg-18 col-md-18 col-sm-18 col-xs-18">
			<h2 class="h2-sp-title">
				<img src="<?php bloginfo('template_url'); ?>/img/content/flow_top_img.jpg" alt="ご注文の流れ" />
				<span class="h2-sp-text">ご注文の流れ</span>
			</h2>
		</div>		
	</div>	
	<div class="row clearfix">
		<div class="col-lg-18 col-md-18 col-sm-18 col-xs-18 mt20">
			<p>こちらでは、お客様がお問い合わせしてから、商品がお手元に届くまでの流れを紹介しております。お問い合わせやお見積もりの前に、ぜひ一度ご確認くださいませ。</p>
		</div>
	</div>
</div><!-- end primary-row -->

<div class="primary-row clearfix">	
	<div class="row clearfix">
        <div class="col-lg-18 col-md-18 col-sm-18 col-xs-18">
			<h3 class="step-to-step">
				<img src="<?php bloginfo('template_url'); ?>/img/content/step2step_img1.jpg" alt="ご注文の流れ" />
				<div class="step-title">お問い合わせ</div>
			</h3>
		</div>
	</div>
	<div class="row clearfix">	
		<div class="col-lg-6 col-md-6 col-sm-6 col-xs-18 xs-align">
			<img src="<?php bloginfo('template_url'); ?>/img/content/flow_content_img1.jpg" alt="ご注文の流れ" />
		</div>
		<div class="col-lg-12 col-md-12 col-sm-12 col-xs-18">
			<p>まずはお気軽にお問い合わせください。<br />
			その際、卓上カレンダーのある程度仕様を決めて頂けると、打ち合わせがスムーズです。<br />
			もし、卓上カレンダーの仕様のことで迷っている項目や疑問点がございましたら、お気軽にお尋ねください。<br />
			価格のご相談や、卓上カレンダー以外のデザイン制作物のご依頼もお待ちしております。<br />
			基本的に即日見積りを目指しています。<br />
			卓上カレンダーの仕様によってはお見積りに時間をいただく場合もございます。ご了承ください。<br />
			デザイン制作が必要な場合は、デザイン費も合わせてお見積りいたします。</p>
		</div>
	</div>
</div>

<div class="primary-row clearfix">	
	<div class="row clearfix">
        <div class="col-lg-18 col-md-18 col-sm-18 col-xs-18">
        	<p><img src="<?php bloginfo('template_url'); ?>/img/content/flow_content_line.jpg" alt="ご注文の流れ" /></p>
        	<p class="mt20 text-center"><img src="<?php bloginfo('template_url'); ?>/img/content/flow_content_arrow.jpg" alt="ご注文の流れ" /></p>
        </div>
    </div>
</div>

<div class="primary-row clearfix">	
	<div class="row clearfix">
        <div class="col-lg-18 col-md-18 col-sm-18 col-xs-18">
			<h3 class="step-to-step">
				<img src="<?php bloginfo('template_url'); ?>/img/content/step2step_img2.jpg" alt="ご注文の流れ" />
				<div class="step-title">お見積り</div>
			</h3>
		</div>
	</div>
	<div class="row clearfix">	
		<div class="col-lg-6 col-md-6 col-sm-6 col-xs-18 xs-align">
			<img src="<?php bloginfo('template_url'); ?>/img/content/flow_content_img2.jpg" alt="ご注文の流れ" />
		</div>
		<div class="col-lg-12 col-md-12 col-sm-12 col-xs-18">
			<p>打ち合わせ内容でお聞きしたご要望より、卓上カレンダーのお見積りをお伝えさせていただきます。仕様を迷われている場合は、複数案のお見積りも可能ですので、お気軽にご相談ください。</p>
		</div>
	</div>
</div>

<div class="primary-row clearfix">	
	<div class="row clearfix">
        <div class="col-lg-18 col-md-18 col-sm-18 col-xs-18">
        	<p><img src="<?php bloginfo('template_url'); ?>/img/content/flow_content_line.jpg" alt="ご注文の流れ" /></p>
        	<p class="mt20 text-center"><img src="<?php bloginfo('template_url'); ?>/img/content/flow_content_arrow.jpg" alt="ご注文の流れ" /></p>
        </div>
    </div>
</div>

<div class="primary-row clearfix">	
	<div class="row clearfix">
        <div class="col-lg-18 col-md-18 col-sm-18 col-xs-18">
			<h3 class="step-to-step">
				<img src="<?php bloginfo('template_url'); ?>/img/content/step2step_img3.jpg" alt="ご注文の流れ" />
				<div class="step-title">ご注文</div>
			</h3>
		</div>
	</div>
	<div class="row clearfix">	
		<div class="col-lg-6 col-md-6 col-sm-6 col-xs-18 xs-align">
			<img src="<?php bloginfo('template_url'); ?>/img/content/flow_content_img3.jpg" alt="ご注文の流れ" />
		</div>
		<div class="col-lg-12 col-md-12 col-sm-12 col-xs-18">
			<p>卓上カレンダーの決定した仕様に基づいての見積りをご提出します。仕様、見積りに納得いただきましたら、正式にご注文となります。</p>
		</div>
	</div>
</div>

<div class="primary-row clearfix">	
	<div class="row clearfix">
        <div class="col-lg-18 col-md-18 col-sm-18 col-xs-18">
        	<p><img src="<?php bloginfo('template_url'); ?>/img/content/flow_content_line.jpg" alt="ご注文の流れ" /></p>
        	<p class="mt20 text-center"><img src="<?php bloginfo('template_url'); ?>/img/content/flow_content_arrow.jpg" alt="ご注文の流れ" /></p>
        </div>
    </div>
</div>

<div class="primary-row clearfix">	
	<div class="row clearfix">
        <div class="col-lg-18 col-md-18 col-sm-18 col-xs-18">
			<h3 class="step-to-step">
				<img src="<?php bloginfo('template_url'); ?>/img/content/step2step_img4.jpg" alt="ご注文の流れ" />
				<div class="step-title">ご注文内容確認・データ入稿</div>
			</h3>
		</div>
	</div>
	<div class="row clearfix">	
		<div class="col-lg-6 col-md-6 col-sm-6 col-xs-18 xs-align">
			<img src="<?php bloginfo('template_url'); ?>/img/content/flow_content_img4.jpg" alt="ご注文の流れ" />
		</div>
		<div class="col-lg-12 col-md-12 col-sm-12 col-xs-18">
			<p>卓上カレンダーに名入れする会社のデザインデータをご入稿ください。<br />
			データ形式はAdobe IllustratorのCCまでを推奨しております。<br />
			Adobe Illustrator以外のデータは別途費用かかる場合がございますので、ご了承ください。<br />
			もし「デザインは苦手」「デザイン制作も一緒に任せたい」ということであれば、弊社で卓上カレンダーのデザイン制作を承ることも可能です。（別途費用）
			卓上カレンダーのプロがお客様のイメージをお伺いし、デザインの制作を承ります。</p>
		</div>
	</div>
</div>

<div class="primary-row clearfix">	
	<div class="row clearfix">
        <div class="col-lg-18 col-md-18 col-sm-18 col-xs-18">
        	<p><img src="<?php bloginfo('template_url'); ?>/img/content/flow_content_line.jpg" alt="ご注文の流れ" /></p>
        	<p class="mt20 text-center"><img src="<?php bloginfo('template_url'); ?>/img/content/flow_content_arrow.jpg" alt="ご注文の流れ" /></p>
        </div>
    </div>
</div>

<div class="primary-row clearfix">	
	<div class="row clearfix">
        <div class="col-lg-18 col-md-18 col-sm-18 col-xs-18">
			<h3 class="step-to-step">
				<img src="<?php bloginfo('template_url'); ?>/img/content/step2step_img5.jpg" alt="ご注文の流れ" />
				<div class="step-title">卓上カレンダー製作</div>
			</h3>
		</div>
	</div>
	<div class="row clearfix">	 
		<div class="col-lg-6 col-md-6 col-sm-6 col-xs-18 xs-align">
			<img src="<?php bloginfo('template_url'); ?>/img/content/flow_content_img5.jpg" alt="ご注文の流れ" />
		</div>
		<div class="col-lg-12 col-md-12 col-sm-12 col-xs-18">
			<p>「卓上名入れカレンダー.com」運営の自社工場により卓上カレンダーの製作を行います。製作された卓上カレンダーは丁寧に検品が行われ出荷されます。卓上カレンダー製造の過程につきましては、こちらのページで詳しく掲載しております。よろしければ、ご覧下さい。「卓上名入れカレンダー.com」運営の自社工場により卓上カレンダーの製作を行います。製作された卓上カレンダーは丁寧に検品が行われ出荷されます。卓上カレンダー製造の過程につきましては、こちらのページで詳しく掲載しております。よろしければ、ご覧下さい。</p>
		</div>
	</div>
</div>

<div class="primary-row clearfix">	
	<div class="row clearfix">
        <div class="col-lg-18 col-md-18 col-sm-18 col-xs-18">
        	<p><img src="<?php bloginfo('template_url'); ?>/img/content/flow_content_line.jpg" alt="ご注文の流れ" /></p>
        	<p class="mt20 text-center"><img src="<?php bloginfo('template_url'); ?>/img/content/flow_content_arrow.jpg" alt="ご注文の流れ" /></p>
        </div>
    </div>
</div>

<div class="primary-row clearfix">	
	<div class="row clearfix">
        <div class="col-lg-18 col-md-18 col-sm-18 col-xs-18">
			<h3 class="step-to-step">
				<img src="<?php bloginfo('template_url'); ?>/img/content/step2step_img6.jpg" alt="ご注文の流れ" />
				<div class="step-title">納品</div>
			</h3>
		</div>
	</div>
	<div class="row clearfix">	
		<div class="col-lg-6 col-md-6 col-sm-6 col-xs-18 xs-align">
			<img src="<?php bloginfo('template_url'); ?>/img/content/flow_content_img6.jpg" alt="ご注文の流れ" />
		</div>
		<div class="col-lg-12 col-md-12 col-sm-12 col-xs-18">
			<p>全国のお客様へ発送、指定日にあわせて納品いたします。</p>
		</div>
	</div>
</div>

<?php get_template_part('part','flow'); ?>    

<?php get_footer();?>