<?php get_header(); ?>
	<div class="primary-row clearfix"><!-- begin primary-row -->
		<div class="row clearfix">
			<div class="col-lg-18 col-md-18 col-sm-18 col-xs-18">
				<h2 class="h2-sp-title">
					<img src="<?php bloginfo('template_url'); ?>/img/content/work_top_img.jpg" alt="制作実績" />
					<span class="h2-sp-text">制作実績</span>
				</h2>
			</div>
		</div>
	</div><!-- end primary-row -->
    <?php query_posts(array( 'paged' => get_query_var('paged') )); ?>
    <?php if (have_posts()) : ?>
        <?php while (have_posts()) : the_post(); ?>    
        <!-- do stuff ... -->
		<div class="primary-row clearfix"><!-- begin primary-row -->	
			<div class="row clearfix">
				<div class="col-lg-18 col-md-18 col-sm-18 col-xs-18">
					<h3 class="work-title"><?php the_title(); ?></h3>					
					<div class="post-row-content clearfix">                
						<div class="post-row-meta">
							<i class="fa fa-clock-o"></i><?php the_time('l, F jS, Y'); ?>
							<i class="fa fa-tags"></i>								
								<?php
								$category = get_the_category();
								echo $category[0]->cat_name;
								?>
							<i class="fa fa-user"></i><span style="color:red;"><?php the_author_link(); ?></span>
						</div>
						<div class="post-row-description"><?php the_content(); ?></div>                    
					</div><!-- end post-row-content -->
				</div>		
			</div>        
		</div><!-- end primary-row -->
        <?php endwhile; ?>    
        <div class="primary-row clearfix">
            <?php if(function_exists('wp_pagenavi')) wp_pagenavi(); ?>
        </div>
    <?php endif; ?>
    <?php wp_reset_query(); ?>    
<?php get_template_part('part','flow');?>
<?php get_footer(); ?>