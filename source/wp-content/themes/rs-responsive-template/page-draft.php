<?php get_header();?>
<div class="primary-row clearfix">	
	<div class="row clearfix">
        <div class="col-lg-18 col-md-18 col-sm-18 col-xs-18">
			<h2 class="h2-sp-title">
			<img src="<?php bloginfo('template_url'); ?>/img/content/draft_top_img.jpg" alt="データ入稿について" />
			<span class="h2-sp-text">データ入稿について</span>
		</h2>
		</div>		
	</div>	
	<div class="row clearfix">
		<div class="col-lg-18 col-md-18 col-sm-18 col-xs-18 mt20">
			<p>下記注意書きをお読みになった上で、お手持ちのデザインデータをご入稿ください。</p>
		</div>
	</div>
</div><!-- end primary-row -->

<div class="primary-row clearfix">	
	<div class="row clearfix">
        <div class="col-lg-18 col-md-18 col-sm-18 col-xs-18">
			<h3 class="title-sub">
				<img src="<?php bloginfo('template_url'); ?>/img/content/title_sub.png" alt="データ入稿について" />
				<div class="title-sub-text">完全データでのご入稿をお願いしております</div>
			</h3>
		</div>
	</div>
	<div class="row clearfix">
        <div class="col-lg-18 col-md-18 col-sm-18 col-xs-18">
			<p>「完全データ」とは、修正の必要がない、完成された印刷可能な制作データのことです。<br/>
				データ入稿の前には、下記注意事項の入念なチェックをお願いいたします。	<br />
				なお、データに不備があった場合、弊社で入稿データの修正をいたします。<br />
				結果、ご入稿日が変わってまいります。<br />
				※受付日や出荷予定日も同様に変わりますので、最初のご入稿日から計算される日にちとは異なります。<br />
				あらかじめご了承ください。<br />
				お客様で入稿データを作成するのが難しい場合は、弊社でイメージのデータ化やデザイン制作も承っております（別途費用）。お気軽にご相談ください。</p>
		</div>
	</div>
</div><!-- end primary-row -->

<div class="primary-row clearfix">	
	<div class="row clearfix">
        <div class="col-lg-18 col-md-18 col-sm-18 col-xs-18">
			<h3 class="title-sub">
				<img src="<?php bloginfo('template_url'); ?>/img/content/title_sub.png" alt="データ入稿について" />
				<div class="title-sub-text">データ入稿での注意事項（必読）</div>
			</h3>
		</div>		
	</div>
	
	<div class="row clearfix">
        <div class="col-lg-18 col-md-18 col-sm-18 col-xs-18">
			<div class="draft-content clearfix">
				<i class="fa fa-tags"></i>
				<p class="draft-info">デザインの原稿は基本的にIllustrator(ai/eps)形式でお送りください。<br />バージョンはCまで対応しております。</p>
			</div>
			<div class="draft-content clearfix">
				<i class="fa fa-tags"></i>
				<p class="draft-info">Illustratorをお持ちでない方に限って、PDFのデータでお送りください。</p>
			</div>
			<div class="draft-content clearfix">
				<i class="fa fa-tags"></i>
				<p class="draft-info">恐れ入りますが、Word、Excel形式のデータはお受けすることが出来かねます。</p>
			</div>
			<div class="draft-content clearfix">
				<i class="fa fa-tags"></i>
				<p class="draft-info">カラーモードはCMYKを選択してください。</p>
			</div>			
			<div class="draft-content clearfix">
				<i class="fa fa-tags"></i>
				<p class="draft-info">解像度は350dpi以上にしてください。解像度が低いと画像が荒くなる可能性があります。</p>
			</div>
			<div class="draft-content clearfix">
				<i class="fa fa-tags"></i>
				<p class="draft-info">Illustratorに配置した画像は全て「埋め込み」してください。</p>
			</div>
			<div class="draft-content clearfix">
				<i class="fa fa-tags"></i>
				<p class="draft-info">フォントは必ず「文字のアウトライン化」をしてください。</p>
			</div>
			<div class="draft-content clearfix">
				<i class="fa fa-tags"></i>
				<p class="draft-info">データの作成は原寸にてお願いいたします。</p>
			</div>
			<div class="draft-content clearfix">
				<i class="fa fa-tags"></i>
				<p class="draft-info">添付ファイルはできるだけ圧縮して、容量は最大3MBくらいにして頂けるようにお願いします。</p>
			</div>		
		</div>		
	</div>
</div><!-- end primary-row -->


<div class="primary-row clearfix">	
	<div class="row clearfix">
        <div class="col-lg-18 col-md-18 col-sm-18 col-xs-18">
			<h3 class="title-sub">
				<img src="<?php bloginfo('template_url'); ?>/img/content/title_sub.png" alt="データ入稿について" />
				<div class="title-sub-text">データ入稿はこちらから</div>
			</h3>
		</div>		
	</div>
	
	<div class="row clearfix">
        <div class="col-lg-18 col-md-18 col-sm-18 col-xs-18">
			<div class="draft-content clearfix">
				<i class="fa fa-tags"></i>
				<p class="draft-info">デザインデータは下記メールに添付して入稿をお願いします。<br />ssk@kintarou.info</p>
			</div>
			<div class="draft-content clearfix">
				<i class="fa fa-tags"></i>
				<p class="draft-info">入稿前に上記注意書きを必ずお読み頂くようにお願いいたします。</p>
			</div>
			<div class="draft-content clearfix">
				<i class="fa fa-tags"></i>
				<p class="draft-info">
					■ <strong>容量の大きいファイルの場合</strong><br />
					（1）4MBを超える容量の大きいデータは極力データを圧縮いただくか、下記データ転送サービスをご利用ください。<br />
					・宅ファイル便（300MBまで無料） http://www.filesend.to/<br />
					・ギガファイル便（容量無制限） http://gigafile.nu/<br />
					（2）デザインデータをCD-ROMもしくはUSBなどに収容し、下記宛先まで郵送でお送りいただく事も可能です。<br />
					※尚お送りいただいたCD-ROMなどのご返却出来かねますので、ご了承ください。<br />
					〒583-0037大阪府藤井寺市津堂2-13-25<br />
					金太郎プリント　卓上名入れカレンダー.com宛
				</p>
			</div>				
		</div>		
	</div>
</div><!-- end primary-row -->


<?php get_template_part('part','flow'); ?>  

<?php get_footer();?>