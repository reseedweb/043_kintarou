<?php get_header();?>
<div class="primary-row clearfix">	
	<div class="row clearfix">
        <div class="col-lg-18 col-md-18 col-sm-18 col-xs-18">
			<h2 class="h2-sp-title">
			<img src="<?php bloginfo('template_url'); ?>/img/content/privacy_top_img.jpg" alt="プライバシーポリシー" />
			<span class="h2-sp-text">プライバシーポリシー</span>
		</h2>
		</div>		
	</div>	
	<div class="row clearfix">
		<div class="col-lg-18 col-md-18 col-sm-18 col-xs-18 mt20">
			<p>卓上名入れカレンダー.comとは、印刷業としての使命と責任を十分に自覚し、個人情報の保護に対して厳重かつ適切な管理体制を敷く責任を負うものと認識して、以下の通り個人情報保護方針を制定し、これを実行、維持、改善いたします。</p>
		</div>
	</div>
</div><!-- end primary-row -->

<div class="primary-row clearfix">	
	<div class="row clearfix">
        <div class="col-lg-18 col-md-18 col-sm-18 col-xs-18">
			<h3 class="step-to-step">
				<img src="<?php bloginfo('template_url'); ?>/img/content/step2step_img1.jpg" alt="プライバシーポリシー" />
				<div class="step-title">個人情報の取得と利用及び第三者提供</div>
			</h3>
		</div>
	</div>
	<div class="row clearfix">	
		<div class="col-lg-18 col-md-18 col-sm-18 col-xs-18">
			<p>当社は個人情報を公正な事業活動に必要な範囲に限定して取得、利用します。また個人情報の取得にあたっては、利用目的を明確にしてその範囲外での利用、第三者への提供はしません。</p>
		</div>
	</div>
</div><!-- end primary-row -->


<div class="primary-row clearfix">	
	<div class="row clearfix">
        <div class="col-lg-18 col-md-18 col-sm-18 col-xs-18">
			<h3 class="step-to-step">
				<img src="<?php bloginfo('template_url'); ?>/img/content/step2step_img2.jpg" alt="プライバシーポリシー" />
				<div class="step-title">個人情報保護に関する法令及び規範の遵守</div>
			</h3>
		</div>
	</div>
	<div class="row clearfix">	
		<div class="col-lg-18 col-md-18 col-sm-18 col-xs-18">
			<p>当社は当社が保有する個人情報の取り扱いに関する法令、国が定める指針、その他の規範を遵守します。</p>
		</div>
	</div>
</div><!-- end primary-row -->


<div class="primary-row clearfix">	
	<div class="row clearfix">
        <div class="col-lg-18 col-md-18 col-sm-18 col-xs-18">
			<h3 class="step-to-step">
				<img src="<?php bloginfo('template_url'); ?>/img/content/step2step_img3.jpg" alt="プライバシーポリシー" />
				<div class="step-title">個人情報の管理</div>
			</h3>
		</div>
	</div>
	<div class="row clearfix">	
		<div class="col-lg-18 col-md-18 col-sm-18 col-xs-18">
			<p>当社は個人情報の管理を厳重に行い漏洩、減失、き損の防止に合理的な安全対策と必要な是正措置を講じます。</p>
		</div>
	</div>
</div><!-- end primary-row -->

<div class="primary-row clearfix">	
	<div class="row clearfix">
        <div class="col-lg-18 col-md-18 col-sm-18 col-xs-18">
			<h3 class="step-to-step">
				<img src="<?php bloginfo('template_url'); ?>/img/content/step2step_img4.jpg" alt="プライバシーポリシー" />
				<div class="step-title">苦情及び相談への対応</div>
			</h3>
		</div>
	</div>
	<div class="row clearfix">	
		<div class="col-lg-18 col-md-18 col-sm-18 col-xs-18">
			<p>当社は個人情報の取り扱いに関して苦情及び相談に適切に対応いたします。</p>
		</div>
	</div>
</div><!-- end primary-row -->

<div class="primary-row clearfix">	
	<div class="row clearfix">
        <div class="col-lg-18 col-md-18 col-sm-18 col-xs-18">
			<h3 class="step-to-step">
				<img src="<?php bloginfo('template_url'); ?>/img/content/step2step_img5.jpg" alt="プライバシーポリシー" />
				<div class="step-title">個人情報保護</div>
			</h3>
		</div>
	</div>
	<div class="row clearfix">	
		<div class="col-lg-18 col-md-18 col-sm-18 col-xs-18">
			<p>マネジメントシステムを制定し、個人情報保護に関する取り組みを継続し、定期的に見直し、改善、維持、向上につとめます。</p>
		</div>
	</div>
</div><!-- end primary-row -->

<div class="primary-row clearfix">	
	<div class="row clearfix">
        <div class="col-lg-18 col-md-18 col-sm-18 col-xs-18">
			<h3 class="step-to-step">
				<img src="<?php bloginfo('template_url'); ?>/img/content/step2step_img6.jpg" alt="プライバシーポリシー" />
				<div class="step-title">お問合せ</div>
			</h3>
		</div>
	</div>
	<div class="row clearfix">	
		<div class="col-lg-18 col-md-18 col-sm-18 col-xs-18">
			<p>発注をいただく際、個人情報の取り扱いに関するご質問がございましたら、下記の「相談窓口」までご連絡ください。<br />
				連絡先／TEL：072-959-3311　FAX：072-959-3321<br />
				担当／谷口　圭代<br />
				受付時間／月曜日～金曜日 9:00～18:00（祝祭日、お盆、年末年始の休業日を除く）</p>
		</div>
	</div>
</div><!-- end primary-row -->

<?php get_template_part('part','flow'); ?>    

<?php get_footer();?>