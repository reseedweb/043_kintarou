<?php get_header();?>
<div class="primary-row clearfix">	
	<div class="row clearfix">
        <div class="col-lg-18 col-md-18 col-sm-18 col-xs-18">
			<h2 class="title-sub">
				<img src="<?php bloginfo('template_url'); ?>/img/content/title_sub.png" alt="タイプを見る" />
				<div class="title-sub-text">タイプを見る</div>
			</h2>
		</div>
	</div>	
	<?php if(have_posts()): while(have_posts()) : the_post(); ?>
	<div class="row clearfix">
        <div class="col-lg-18 col-md-18 col-sm-18 col-xs-18">			
			<ul class="type-tabs-detail"><!-- begin type-tabs-detail-->
				<li><a href="<?php bloginfo('url'); ?>/types">ALL</a></li>					
				<li><a href="<?php bloginfo('url'); ?>/cat-types/ケース">ケース</a></li>					
				<li><a href="<?php bloginfo('url'); ?>/cat-types/三角スタンド">三角スタンド</a></li>
				<li><a href="<?php bloginfo('url'); ?>/cat-types/テレフォン">テレフォン</a></li>									
			</ul><!-- end type-tabs-detail -->
		</div>
	</div>
	
	<div class="type-detail-content clearfix">
		<div class="row clearfix">
			<div class="col-lg-18 col-md-18 col-sm-18 col-xs-18">
				<h3 class="type-dtitle-h31">
					<img src="<?php bloginfo('template_url'); ?>/img/content/type_dtitle_h31.jpg" alt="タイプを見る" />
					<div class="type-h31-text"><?php @the_terms($type_post->ID, 'cat-types'); ?></div>
				</h3>
			</div>
		</div>
			<div class="row clearfix">
				<div class="col-lg-18 col-md-18 col-sm-18 col-xs-18">
					<h4 class="type-dtitle-h4"><?php echo the_title(); ?></h4>
				</div>
			</div>
			<div class="row clearfix">
				<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12" id="mainalbum">
					<span><img src="<?php the_field('image1', get_the_id()); ?>" class="mainImage" /></span>
				</div>
				<div class="col-lg-6 col-md-6 col-sm-6 col-xs-6" id="subalbum">										
					<div class="row clearfix">
						<div class="col-lg-9 col-md-9 col-sm-9 col-xs-9">
							<img src="<?php the_field('image1', get_the_id()); ?>" class="thumb" name="mainImage" />	
						</div>
						<div class="col-lg-9 col-md-9 col-sm-9 col-xs-9">
							<img src="<?php the_field('image2', get_the_id()); ?>" class="thumb" name="mainImage" />	
						</div>
					</div>	
					
					<div class="row mt20 clearfix">
						<div class="col-lg-9 col-md-9 col-sm-9 col-xs-9">
							<img src="<?php the_field('image3', get_the_id()); ?>" class="thumb" name="mainImage" />	
						</div>
						<div class="col-lg-9 col-md-9 col-sm-9 col-xs-9">
							<img src="<?php the_field('image4', get_the_id()); ?>" class="thumb" name="mainImage" />	
						</div>
					</div>								
					
					<div class="row mt20 clearfix">
						<div class="col-lg-9 col-md-9 col-sm-9 col-xs-9">
							<img src="<?php the_field('image5', get_the_id()); ?>" class="thumb" name="mainImage" />	
						</div>
						<div class="col-lg-9 col-md-9 col-sm-9 col-xs-9">
							<img src="<?php the_field('image6', get_the_id()); ?>" class="thumb" name="mainImage" />	
						</div>
					</div>	
					
					<div class="row mt20 clearfix">
						<div class="col-lg-9 col-md-9 col-sm-9 col-xs-9">
							<img src="<?php the_field('image7', get_the_id()); ?>" class="thumb" name="mainImage" />	
						</div>
						<div class="col-lg-9 col-md-9 col-sm-9 col-xs-9">
							<img src="<?php the_field('image8', get_the_id()); ?>" class="thumb" name="mainImage" />	
						</div>
					</div>						
				</div>
			</div>
			<div class="row clearfix">
				<div class="col-lg-18 col-md-18 col-sm-18 col-xs-18">	
					<div class="type-ttop-text">
						<?php echo get_field('text1', get_the_id()); ?>
					</div>				
				</div>
			</div>
			<div class="row clearfix">
				<div class="col-lg-18 col-md-18 col-sm-18 col-xs-18">
					<h3 class="type-dtitle-h32">
						<img src="<?php bloginfo('template_url'); ?>/img/content/type_dtitle_h32.jpg" alt="タイプを見る" />
						<div class="type-h32-text">エコペーパーリング製品 | 特許 第4528822号 他 国際特許取得済</div>
					</h3>
					<table class="type-table-info">
						<tr>
							<td>サイズ</td>
							<td><?php echo get_field('text2', get_the_id()); ?></td>
						</tr>	
						<tr>
							<td>金額（1冊）</td>
							<td><?php echo get_field('text3', get_the_id()); ?>円</td>
						</tr>
						<tr>
							<td>用紙</td>
							<td>用紙 <?php echo get_field('text4', get_the_id()); ?>kg</td>
						</tr>
						<tr>
							<td>名入れ印刷寸法</td>
							<td><?php echo get_field('text5', get_the_id()); ?>以内</td>
						</tr>
						<tr>
							<td>名入れ方法</td>
							<td><?php echo get_field('text6', get_the_id()); ?></td>
						</tr>						
					</table>
				</div>
			</div>	
	</div>
	
	<div class="primary-row text-center">
	    <?php if(function_exists('wp_pagenavi')) { wp_pagenavi(); } ?>
	</div>
	<?php wp_reset_query(); ?>		
	<?php endwhile;endif; ?>	
</div><!-- end primary-row -->

<?php get_template_part('part','flow'); ?> 
<?php get_footer();?>