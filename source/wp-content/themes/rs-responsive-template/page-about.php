<?php get_header();?>
<div class="primary-row clearfix">	
	<div class="row clearfix">
        <div class="col-lg-18 col-md-18 col-sm-18 col-xs-18">
			<h2 class="h2-sp-title">
			<img src="<?php bloginfo('template_url'); ?>/img/content/about_top_img.jpg" alt="初めての方へ" />
			<span class="h2-sp-text">初めての方へ</span>
		</h2>
		</div>		
	</div>	
	<div class="row clearfix">
		<div class="col-lg-18 col-md-18 col-sm-18 col-xs-18 mt20">
			<p>当社の強みはお客様ひとりひとりにあわせた提案力です！<br />
				卓上名入れカレンダー.comでは、「小ロット・短納期で柔軟に対応」「経験豊富なオーナーがご提案」「確かな技術力でどんなものにも印刷」この3つを柱に、ヒアリングしたご要望、サイズ・形状・デザイン・素材にいたるまで、プロの提案ベースでお任せいただけるので、内容物にあわせてぴったりのオリジナルカレンダーが制作できます！</p>
		</div>
	</div>
</div><!-- end primary-row -->

<div class="primary-row clearfix">
	<div class="row clearfix">
		<div class="col-lg-18 col-md-18 col-sm-18 col-xs-18">
			<div class="about-content-top clearfix">
				<img src="<?php bloginfo('template_url'); ?>/img/content/about_content_top.jpg" alt="初めての方へ" />
				<h3 class="about-contop-title">こんな悩みは<br />ございませんか?</h3>
				<div class="about-contop-ltitle">お悩み</div>
				<div class="about-contop-rtitle">解決策</div>
				<ul class="about-contop-ltext">
					<li>サイズ・形状がわからない</li>
					<li>思った通りにカレンダーを作りたい</li>
					<li>コストを抑えたい</li>
					<li>納期に間に合わせたい</li>
				</ul>
				<ul class="about-contop-rtext">
					<li>内容物や使用するシチュエーションなどをヒアリングし、<span class="about-rtext-clr">最適なサイズ、形状をご提案</span>いたします。</li>
					<li>どのような化粧箱をご希望でしょうか？イメージや参考の商品・ブランドをお教えください。デザイン製作もお任せいただける（別途費用）ので、<span class="about-rtext-clr">完全オリジナルの化粧箱製作が可能</span>です。</li>
					<li>コスト削減のご相談もお任せください。紙の素材や形状などをお任せいただければ、<span class="about-rtext-clr">最安値でのお見積りも可能</span>です。お問合せの際にご予算などもお伝えください。</li>
					<li>ご希望の納期に沿って製作いたします。特にお急ぎの方は当社オリジナルカレンダーの中からお選びいただければ、より<span class="about-rtext-clr">短納期での製作も可能</span>な場合がございます。まずはご相談ください。</li>
				</ul>
			</div>
			<div class="about-content-top-sp">
				<h3 class="about-consp-title">こんな悩みはございませんか?</h3>				
				<div class="clearfix">
					<div class="about-consp-ltitle">お悩み</div>				
					<div class="about-consp-rtitle">解決策</div>
				</div>				
				<ul class="about-consp-ltext">					
					<li><img src="<?php bloginfo('template_url'); ?>/img/content/about_img1.png" alt="初めての方へ" /><span class="about-consp-text">サイズ・形状がわからない</span></li>
					<li><img src="<?php bloginfo('template_url'); ?>/img/content/about_img2.png" alt="初めての方へ" /><span class="about-consp-text">思った通りにカレンダーを作りたい</span></li>
					<li><img src="<?php bloginfo('template_url'); ?>/img/content/about_img3.png" alt="初めての方へ" /><span class="about-consp-text">コストを抑えたい</span></li>
					<li><img src="<?php bloginfo('template_url'); ?>/img/content/about_img4.png" alt="初めての方へ" /><span class="about-consp-text">納期に間に合わせたい</span></li>
				</ul>								
				<ul class="about-consp-rtext">					
					<li>内容物や使用するシチュエーションなどをヒアリングし、<span class="about-consp-clr">最適なサイズ、形状をご提案</span>いたします。</li>
					<li>どのような化粧箱をご希望でしょうか？イメージや参考の商品・ブランドをお教えください。デザイン製作もお任せいただける（別途費用）ので、<span class="about-consp-clr">完全オリジナルの化粧箱製作が可能</span>です。</li>
					<li>どのような化粧箱をご希望でしょうか？イメージや参考の商品・ブランドをお教えください。デザイン製作もお任せいただける（別途費用）ので、<span class="about-consp-clr">完全オリジナルの化粧箱製作が可能</span>です。</li>
					<li>ご希望の納期に沿って製作いたします。特にお急ぎの方は当社オリジナルカレンダーの中からお選びいただければ、より<span class="about-consp-clr">短納期での製作も可能</span>な場合がございます。まずはご相談ください。</li>
				</ul>
			</div>
		</div>		
	</div>	
</div>

<div class="primary-row clearfix">	
	<div class="row clearfix">
        <div class="col-lg-18 col-md-18 col-sm-18 col-xs-18">
			<h3 class="title-sub">
				<img src="<?php bloginfo('template_url'); ?>/img/content/title_sub.png" alt="初めての方へ" />
				<div class="title-sub-text">確かな技術力でどんなものにも印刷します</div>
			</h3>
		</div>
	</div>
	<div class="row clearfix">	
		<div class="col-lg-6 col-md-6 col-sm-6 col-xs-18 xs-align">
			<img src="<?php bloginfo('template_url'); ?>/img/content/about_content_img1.jpg" alt="初めての方へ" />
		</div>
		<div class="col-lg-12 col-md-12 col-sm-12 col-xs-18">
			<p>卓上名入れカレンダー.comでは、特殊印刷の技術で曲面や凹凸面、布地やプラスチック、ガラスなど、どこにでも印刷することもでき、小さな文字もきれいに印刷できます。</p>
		</div>
	</div>
</div>

<div class="primary-row clearfix">	
	<div class="row clearfix">
        <div class="col-lg-18 col-md-18 col-sm-18 col-xs-18">
			<h3 class="title-sub">
				<img src="<?php bloginfo('template_url'); ?>/img/content/title_sub.png" alt="初めての方へ" />
				<div class="title-sub-text">小ロット・短納期で柔軟柔軟に対応しております</div>
			</h3>
		</div>
	</div>
	<div class="row clearfix">	
		<div class="col-lg-6 col-md-6 col-sm-6 col-xs-18 xs-align">
			<img src="<?php bloginfo('template_url'); ?>/img/content/about_content_img2.jpg" alt="初めての方へ" />
		</div>
		<div class="col-lg-12 col-md-12 col-sm-12 col-xs-18">
			<p>卓上名入れカレンダー.comでは、安定した品質の製品を1冊000円～という安価、最短○週間～という短納期で製作することができます！サンプル製作も承っております。</p>
		</div>
	</div>
</div>

<div class="primary-row clearfix">	
	<div class="row clearfix">
        <div class="col-lg-18 col-md-18 col-sm-18 col-xs-18">
			<h3 class="title-sub">
				<img src="<?php bloginfo('template_url'); ?>/img/content/title_sub.png" alt="初めての方へ" />
				<div class="title-sub-text">経験豊富なオーナーが直接ご提案いたします！</div>
			</h3>
		</div>
	</div>
	<div class="row clearfix">	
		<div class="col-lg-6 col-md-6 col-sm-6 col-xs-18 xs-align">
			<img src="<?php bloginfo('template_url'); ?>/img/content/about_content_img2.jpg" alt="初めての方へ" />
		</div>
		<div class="col-lg-12 col-md-12 col-sm-12 col-xs-18">
			<p>実現できるか分からないことや、こんな商品に印刷できるだろうか？と思ったら、まずご相談ください。<br />サンプル印刷からお引き受けいたします。</p>
		</div>
	</div>
</div>

<?php get_template_part('part','flow'); ?>  

<?php get_footer();?>