<?php get_header();?>
    <div class="primary-row clearfix"><!-- begin primary-row -->
		<div class="row clearfix">
			<div class="col-lg-18 col-md-18 col-sm-18 col-xs-18">
				<h3 class="title-sub">
					<img src="<?php bloginfo('template_url'); ?>/img/content/title_sub.png" alt="お問い合わせ" />
					<div class="title-sub-text">お問い合わせ</div>
				</h3>
				<p>
					お問い合わせ・お見積もりありがとうございました。専門スタッフがメールを確認後、即日対応させて頂きます。（営業時間終了後の場合はご返信は翌営業日になります。ご了承ください）
					１週間たっても返信がない場合、お急ぎの場合は、お手数ですが下記電話番号までご連絡くださいませ。
				</p>
			</div>		
		</div>		
    </div><!-- end primary-row -->	
	<?php get_template_part('part','flow'); ?>  
<?php get_footer();?>